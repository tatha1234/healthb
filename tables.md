// inspection table
abnormality
act_arr
act_dep
alp_desig
alp_hq
alp_name
alp_observ
alp_pme
alp_tech
alp_tran
arr_remark
author
dateCreated
dateLastChanged
dep_remark
description
dest
emp_id
eng_abnormality
gd_hq
gd_name
inspection_date
inspection_place
inspection_type
locations
loco_base
loco_last_insp_date
loco_last_insp_type
loco_no
loco_observ
loco_poh
loco_type
lp_abnormality
lp_cli
lp_cli_last_fp
lp_desig
lp_grade
lp_hq
lp_name
lp_observ
lp_pme
lp_tech
lp_tech_counseling
lp_tran
lp_tranp_counseling
org
sch_arr
sch_dep
st_abnormality
traffic_abnormality
train_no
trd_abnormality
uid


location table

latitude
loc_address
loc_desc
loc_time
longitude

abnormality

author
inspection_uid
report
report_date
report_place
report_type
uid

gradation

dateCreated
grade_date
text10
text11
text12
text13
text14
text16
text17
text18
text2
text3
text4
text5
text6
text7
text8
text9

post

ALP
author
body
from
LPG
LPM
LPP
LPS
notice_date
reference
starCount
title
to
uid

post_counselling

ALP
author
counsel_date
counsel_place
dateCreated
dateLastChanged
LPG
LPM
LPP
LPS
text
uid


feedback summary

ALP_FP
author
Automatic_Ambush
BA_Test
Brake_Feel_Ambush
Brake_Power_Ambush
Cab_Checking
Data_Analysis
Flasher_Ambush
Foot_Plate
Gate_Ambush
Gate_Inspection
JoinSig_Sighting
Lobby_Inspection
LPG_A
LPG_B
LPG_C
LPM_A
LPM_B
LPM_C
LPP_A
LPP_B
LPP_C
LPS_A
LPS_B
LPS_C
Other_Inspection
Platform_Duty
Running_Room_Inspection
Shed_Visit

supervisor table

cliname
cug
desig
division
email
inspection_date
inspection_place
inspection_type
lastSeenTime
regtoken
user_type
username
zone