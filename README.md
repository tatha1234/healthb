# health
mysql -u root -p traccar_db < desig.sql
ALTER TABLE `traccar_db`.`health_status`
CHANGE COLUMN `health_status` `health_status` TEXT NULL DEFAULT NULL ;

ALTER TABLE `traccar_db`.`desig_srl`
ADD COLUMN `C_NEW_DESIG_SHORTEST` VARCHAR(45) NULL AFTER `NEW_DESIG_SHORTEST`,
ADD COLUMN `C_NEW_DESIG_SHORT` VARCHAR(45) NULL AFTER `C_NEW_DESIG_SHORTEST`;

INSERT INTO `traccar_db`.`desig_srl` (`SRL_NO`, `SRL_NO1`, `DESIG`, `C_DESIG`, `C_DESIG_NEW`, `DESIG_SRL`, `NEW_DESIG`, `NEW_DESIG_SHORT`, `NEW_DESIG_SHORTEST`, `C_NEW_DESIG_SHORTEST`, `C_NEW_DESIG_SHORT`) VALUES ('2', '2', 'MOTORMAN', 'MOTORMAN', 'MOTORMAN', '2', 'MOTORMAN', 'MOTORMAN', 'MOTORMAN', 'MOTORMAN', 'MOTORMAN');

UPDATE `traccar_db`.`employees` set `Present Designation` = 'MOTORMAN' WHERE `Present Designation` IN ('PASS DRIVER','SR. PASS DRIVER');

ALTER TABLE `traccar_db`.`train_master`
CHANGE COLUMN `train_name` `train_name` TEXT CHARACTER SET 'latin1' COLLATE 'latin1_general_ci' NULL ,
CHANGE COLUMN `train_type` `train_type` TEXT CHARACTER SET 'latin1' COLLATE 'latin1_general_ci' NULL ,
CHANGE COLUMN `valid_from` `valid_from` DATE NULL ,
CHANGE COLUMN `valid_to` `valid_to` DATE NULL ,
CHANGE COLUMN `zone` `zone` TEXT CHARACTER SET 'latin1' COLLATE 'latin1_general_ci' NULL ,
CHANGE COLUMN `divn` `divn` TEXT CHARACTER SET 'latin1' COLLATE 'latin1_general_ci' NULL ,
CHANGE COLUMN `depot` `depot` TEXT CHARACTER SET 'latin1' COLLATE 'latin1_general_ci' NULL ,
CHANGE COLUMN `days_of_run` `days_of_run` TEXT CHARACTER SET 'latin1' COLLATE 'latin1_general_ci' NULL ,
CHANGE COLUMN `dep` `dep` TIME NULL ,
CHANGE COLUMN `org` `org` TEXT CHARACTER SET 'latin1' COLLATE 'latin1_general_ci' NULL ,
CHANGE COLUMN `dest` `dest` TEXT CHARACTER SET 'latin1' COLLATE 'latin1_general_ci' NULL ,
CHANGE COLUMN `arr` `arr` TIME NULL ,
CHANGE COLUMN `travel_time` `travel_time` TIME NULL ,
CHANGE COLUMN `total_kms` `total_kms` DECIMAL(10,0) NULL ,
CHANGE COLUMN `avg_speed` `avg_speed` DECIMAL(10,0) NULL ,
CHANGE COLUMN `er` `er` INT(2) NULL ,
CHANGE COLUMN `tr` `tr` INT(2) NULL ;

INSERT INTO `traccar_db`.`train_master`
(`train_no`,
`train_name`,
`dep`,
`org`,
`dest`,
`arr`
)
select `train_no`,
`train_name`,
`dep`,
`org`,
`dest`,
`arr`
from traccar_db.train_master_n;


ALTER TABLE `traccar_db`.`time_table`
CHANGE COLUMN `stn_name` `stn_name` TEXT CHARACTER SET 'latin1' COLLATE 'latin1_general_ci' NULL ,
CHANGE COLUMN `stn_type` `stn_type` TEXT CHARACTER SET 'latin1' COLLATE 'latin1_general_ci' NULL ,
CHANGE COLUMN `stn_code` `stn_code` TEXT CHARACTER SET 'latin1' COLLATE 'latin1_general_ci' NULL ,
CHANGE COLUMN `division` `division` TEXT CHARACTER SET 'latin1' COLLATE 'latin1_general_ci' NULL ,
CHANGE COLUMN `zone` `zone` TEXT CHARACTER SET 'latin1' COLLATE 'latin1_general_ci' NULL ,
CHANGE COLUMN `block_section` `block_section` TEXT CHARACTER SET 'latin1' COLLATE 'latin1_general_ci' NULL ,
CHANGE COLUMN `wtt_arr` `wtt_arr` TIME NULL ,
CHANGE COLUMN `wtt_day_arr` `wtt_day_arr` INT(11) NULL ,
CHANGE COLUMN `wtt_dep` `wtt_dep` TIME NULL ,
CHANGE COLUMN `wtt_day_dep` `wtt_day_dep` INT(11) NULL ,
CHANGE COLUMN `stop` `stop` TEXT CHARACTER SET 'latin1' COLLATE 'latin1_general_ci' NULL ,
CHANGE COLUMN `ptt_arr` `ptt_arr` TIME NULL ,
CHANGE COLUMN `ptt_day_arr` `ptt_day_arr` INT(11) NULL ,
CHANGE COLUMN `ptt_dep` `ptt_dep` TIME NULL ,
CHANGE COLUMN `ptt_day_dep` `ptt_day_dep` INT(11) NULL ,
CHANGE COLUMN `pf` `pf` TEXT CHARACTER SET 'latin1' COLLATE 'latin1_general_ci' NULL ,
CHANGE COLUMN `tr` `tr` INT(11) NULL ,
CHANGE COLUMN `er` `er` INT(11) NULL ,
CHANGE COLUMN `distance` `distance` DOUBLE NULL ,
CHANGE COLUMN `stn_sr_no` `stn_sr_no` INT(11) NULL ;

INSERT INTO `traccar_db`.`time_table`
(`train_no`,
`stn_code`,
`block_section`,
`wtt_arr`,
`wtt_dep`,
`stn_sr_no`)
select
`train_no`,
`stn_code`,
`block_section`,
`wtt_arr`,
`wtt_dep`,
`stn_sr_no`
from `traccar_db`.`time_table_n`;


LOAD DATA INFILE "C:\Users\Hp\OneDrive\Documents\shednotice_all.csv"
INTO TABLE shednotice
COLUMNS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;