-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: localhost    Database: cccrsdah
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abnormality`
--

DROP TABLE IF EXISTS `abnormality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `abnormality` (
  `author` varchar(45) DEFAULT NULL,
  `inspection_uid` varchar(45) DEFAULT NULL,
  `report` varchar(45) DEFAULT NULL,
  `report_date` date DEFAULT NULL,
  `report_place` varchar(45) DEFAULT NULL,
  `report_type` varchar(45) DEFAULT NULL,
  `uid` varchar(45) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abnormality`
--

LOCK TABLES `abnormality` WRITE;
/*!40000 ALTER TABLE `abnormality` DISABLE KEYS */;
INSERT INTO `abnormality` VALUES ('ABC',NULL,'ok','2009-12-20','DDJ to HNB to DDJ','TRD','123');
/*!40000 ALTER TABLE `abnormality` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback summary`
--

DROP TABLE IF EXISTS `feedback summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feedback summary` (
  `ALP_FP` varchar(45) NOT NULL,
  `author` varchar(45) DEFAULT NULL,
  `Automatic_Ambush` varchar(45) DEFAULT NULL,
  `BA_Test` varchar(45) DEFAULT NULL,
  `Brake_Feel_Ambush` varchar(45) DEFAULT NULL,
  `Brake_Power_Ambush` varchar(45) DEFAULT NULL,
  `Cab_Checking` varchar(45) DEFAULT NULL,
  `Data_Analysis` varchar(45) DEFAULT NULL,
  `Flasher_Ambush` varchar(45) DEFAULT NULL,
  `Foot_Plate` varchar(45) DEFAULT NULL,
  `Gate_Ambush` varchar(45) DEFAULT NULL,
  `JoinSig_Sighting` varchar(45) DEFAULT NULL,
  `Lobby_Inspection` varchar(45) DEFAULT NULL,
  `LPG_A` varchar(45) DEFAULT NULL,
  `LPG_B` varchar(45) DEFAULT NULL,
  `LPG_C` varchar(45) DEFAULT NULL,
  `LPM_A` varchar(45) DEFAULT NULL,
  `LPM_B` varchar(45) DEFAULT NULL,
  `LPM_C` varchar(45) DEFAULT NULL,
  `LPP_A` varchar(45) DEFAULT NULL,
  `LPP_B` varchar(45) DEFAULT NULL,
  `LPP_C` varchar(45) DEFAULT NULL,
  `LPS_A` varchar(45) DEFAULT NULL,
  `LPS_B` varchar(45) DEFAULT NULL,
  `LPS_C` varchar(45) DEFAULT NULL,
  `Other_Inspection` varchar(45) DEFAULT NULL,
  `Platform_Duty` varchar(45) DEFAULT NULL,
  `Running_Room_Inspection` varchar(45) DEFAULT NULL,
  `Shed_Visit` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ALP_FP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback summary`
--

LOCK TABLES `feedback summary` WRITE;
/*!40000 ALTER TABLE `feedback summary` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback summary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gradation`
--

DROP TABLE IF EXISTS `gradation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gradation` (
  `dateCreated` date NOT NULL,
  `grade_date` date DEFAULT NULL,
  `text10` varchar(45) DEFAULT NULL,
  `text11` varchar(45) DEFAULT NULL,
  `text12` varchar(45) DEFAULT NULL,
  `text13` varchar(45) DEFAULT NULL,
  `text14` varchar(45) DEFAULT NULL,
  `text16` varchar(45) DEFAULT NULL,
  `text17` varchar(45) DEFAULT NULL,
  `text18` varchar(45) DEFAULT NULL,
  `text2` varchar(45) DEFAULT NULL,
  `text3` varchar(45) DEFAULT NULL,
  `text4` varchar(45) DEFAULT NULL,
  `text5` varchar(45) DEFAULT NULL,
  `text6` varchar(45) DEFAULT NULL,
  `text7` varchar(45) DEFAULT NULL,
  `text8` varchar(45) DEFAULT NULL,
  `text9` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`dateCreated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gradation`
--

LOCK TABLES `gradation` WRITE;
/*!40000 ALTER TABLE `gradation` DISABLE KEYS */;
/*!40000 ALTER TABLE `gradation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inspection`
--

DROP TABLE IF EXISTS `inspection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `inspection` (
  `emp_id` int DEFAULT NULL,
  `abnormality` varchar(45) DEFAULT NULL,
  `act_arr` time DEFAULT NULL,
  `act_dep` time DEFAULT NULL,
  `alp_desig` varchar(45) DEFAULT NULL,
  `alp_hq` varchar(45) DEFAULT NULL,
  `alp_name` varchar(45) DEFAULT NULL,
  `alp_observ` varchar(45) DEFAULT NULL,
  `alp_pme` date DEFAULT NULL,
  `alp_tech` date DEFAULT NULL,
  `alp_tran` date DEFAULT NULL,
  `arr_remark` varchar(45) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `dateCreated` date DEFAULT NULL,
  `dateLastChanged` date DEFAULT NULL,
  `dep_remark` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `dest` varchar(45) DEFAULT NULL,
  `eng_abnormality` varchar(45) DEFAULT NULL,
  `gd_hq` varchar(45) DEFAULT NULL,
  `gd_name` varchar(45) DEFAULT NULL,
  `inspection_date` date DEFAULT NULL,
  `inspection_place` varchar(45) DEFAULT NULL,
  `inspection_type` varchar(45) DEFAULT NULL,
  `locations` varchar(45) DEFAULT NULL,
  `loco_base` varchar(45) DEFAULT NULL,
  `loco_last_insp_date` date DEFAULT NULL,
  `loco_last_insp_type` varchar(45) DEFAULT NULL,
  `loco_no` varchar(45) DEFAULT NULL,
  `loco_observ` varchar(45) DEFAULT NULL,
  `loco_poh` varchar(45) DEFAULT NULL,
  `loco_type` varchar(45) DEFAULT NULL,
  `lp_abnormality` varchar(45) DEFAULT NULL,
  `lp_cli` varchar(45) DEFAULT NULL,
  `lp_cli_last_fp` varchar(45) DEFAULT NULL,
  `lp_desig` varchar(45) DEFAULT NULL,
  `lp_grade` varchar(45) DEFAULT NULL,
  `lp_hq` varchar(45) DEFAULT NULL,
  `lp_name` varchar(45) DEFAULT NULL,
  `lp_observ` varchar(45) DEFAULT NULL,
  `lp_pme` date DEFAULT NULL,
  `lp_tech` date DEFAULT NULL,
  `lp_tech_counseling` varchar(45) DEFAULT NULL,
  `lp_tran` date DEFAULT NULL,
  `lp_tranp_counseling` varchar(45) DEFAULT NULL,
  `org` varchar(45) DEFAULT NULL,
  `sch_arr` time DEFAULT NULL,
  `sch_dep` time DEFAULT NULL,
  `st_abnormality` varchar(45) DEFAULT NULL,
  `traffic_abnormality` varchar(45) DEFAULT NULL,
  `train_no` int DEFAULT NULL,
  `trd_abnormality` varchar(45) DEFAULT NULL,
  `uid` varchar(45) NOT NULL,
  `cli_name` varchar(45) DEFAULT NULL,
  `section` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inspection`
--

LOCK TABLES `inspection` WRITE;
/*!40000 ALTER TABLE `inspection` DISABLE KEYS */;
INSERT INTO `inspection` VALUES (NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ABC',NULL,NULL,NULL,NULL,'2021-11-10',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','abc','BP SDAH','EMU Footplate');
/*!40000 ALTER TABLE `inspection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `post` (
  `ALP` varchar(45) DEFAULT NULL,
  `uid` varchar(45) NOT NULL,
  `author` varchar(45) DEFAULT NULL,
  `body` varchar(45) DEFAULT NULL,
  `from` date DEFAULT NULL,
  `LPG` varchar(45) DEFAULT NULL,
  `LPM` varchar(45) DEFAULT NULL,
  `LPP` varchar(45) DEFAULT NULL,
  `LPS` varchar(45) DEFAULT NULL,
  `notice_date` date DEFAULT NULL,
  `reference` varchar(45) DEFAULT NULL,
  `starCount` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `to` date DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_counselling`
--

DROP TABLE IF EXISTS `post_counselling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `post_counselling` (
  `uid` varchar(45) NOT NULL,
  `ALP` varchar(45) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `counsel_date` date DEFAULT NULL,
  `counsel_place` varchar(45) DEFAULT NULL,
  `dateCreated` date DEFAULT NULL,
  `dateLastChanged` date DEFAULT NULL,
  `LPG` varchar(45) DEFAULT NULL,
  `LPM` varchar(45) DEFAULT NULL,
  `LPP` varchar(45) DEFAULT NULL,
  `LPS` varchar(45) DEFAULT NULL,
  `text` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_counselling`
--

LOCK TABLES `post_counselling` WRITE;
/*!40000 ALTER TABLE `post_counselling` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_counselling` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-15 14:16:07
