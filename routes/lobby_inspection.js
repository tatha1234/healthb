var express = require('express');
var router = express.Router();
var passport = require('passport');
require('../config/passport')(passport);
var pool = require('../config/mysqlconnpool').pool;
var moment = require('moment');
var _ = require('lodash');
var util = require('./../modules/util');

/* GET ALL BOOKS */
router.get('/', function (req, res) {
  // var token = getToken(req.headers);
  // if (token) {
  pool.query(`SELECT * from clifb_lobby_inspection`, function (err, results, fields) {
    if (err) {
      return res.status(401).send({
        success: false,
        msg: err
      });
    }
    return res.json(results);
  });
  // } else {
  //   return res.status(403).send({
  //     success: false,
  //     msg: 'Unauthorized.'
  //   });
  // }
});

/* GET SINGLE BOOK BY ID */
router.get('/id', function (req, res, next) {
  pool.query(`SELECT * from clifb_lobby_inspection where id = ?`, [req.query.id], function (err, results, fields) {
    if (err) {
      return res.status(401).send({
        success: false,
        msg: err
      });
    }
    console.log(results);
    return res.json(results);
  });
});

/* GET SINGLE BOOK BY ID */
router.post('/', function (req, res, next) {
  // console.log(req)
  if (req.body) {
    var lobby_inspetion = {
      insp_list_id: req.body.insp_list_id,
      abnormality_id: req.body.abnormality_id,
      location_id: req.body.location_id,
      lobby_name: req.body.lobby_name,
      category: req.body.category,
      name_onduty_crewcontroller: req.body.name_onduty_crewcontroller,
      combined: req.body.combined,
      size: req.body.size,
      ventilation_lighting: req.body.ventilation_lighting,
      furniture: req.body.furniture,
      water: req.body.water,
      coolers: req.body.coolers,
      power_backup: req.body.power_backup,
      phone: req.body.phone,
      attached_restroom: req.body.attached_restroom,
      lockers: req.body.lockers,
      registers: req.body.registers,
      notice_book: req.body.notice_book,
      track_signal_defect: req.body.track_signal_defect,
      safety_notice: req.body.safety_notice,
      speed_board: req.body.speed_board,
      ba: req.body.ba,
      cms_ss: req.body.cms_ss,
      spectacle_board: req.body.spectacle_board,
      staff_avl_board: req.body.staff_avl_board,
      suspected_drinkers: req.body.suspected_drinkers,
      list_pme_due: req.body.list_pme_due,
      list_ref_due: req.body.list_ref_due,
      observation: req.body.observation
    
    }
    console.log(req.body),
    pool.query('INSERT INTO clifb_lobby_inspection SET ?', lobby_inspetion, function (err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      //  console.log(results);
      return res.json(results);
    });
  }
})

router.get('/id', function (req, res, next) {
  pool.query(`SELECT * from clifb_lobby_inspection where id = ?`, [req.query.id], function (err, results, fields) {
    if (err) {
      return res.status(401).send({
        success: false,
        msg: err
      });
    }
    console.log(results);
    return res.json(results);
  });
});

/* GET SINGLE BOOK BY ID */
router.put('/', function (req, res, next) {
  // console.log(req)
  if (req.body) {
    var lobby_inspetion = {
      id : req.body.id,
      insp_list_id: req.body.insp_list_id,
      abnormality_id: req.body.abnormality_id,
      location_id: req.body.location_id,
      lobby_name: req.body.lobby_name,
      category: req.body.category,
      name_onduty_crewcontroller: req.body.name_onduty_crewcontroller,
      combined: req.body.combined,
      size: req.body.size,
      ventilation_lighting: req.body.ventilation_lighting,
      furniture: req.body.furniture,
      water: req.body.water,
      coolers: req.body.coolers,
      power_backup: req.body.power_backup,
      phone: req.body.phone,
      attached_restroom: req.body.attached_restroom,
      lockers: req.body.lockers,
      registers: req.body.registers,
      notice_book: req.body.notice_book,
      track_signal_defect: req.body.track_signal_defect,
      safety_notice: req.body.safety_notice,
      speed_board: req.body.speed_board,
      ba: req.body.ba,
      cms_ss: req.body.cms_ss,
      spectacle_board: req.body.spectacle_board,
      staff_avl_board: req.body.staff_avl_board,
      suspected_drinkers: req.body.suspected_drinkers,
      list_pme_due: req.body.list_pme_due,
      list_ref_due: req.body.list_ref_due,
      observation: req.body.observation
    
    }
    console.log(req.body),
    pool.query('UPDATE clifb_lobby_inspection SET ? WHERE id = ?', [lobby_inspetion , lobby_inspetion.id], function (err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      //  console.log(results);
      return res.json(results);
    });
  }
})





// router.put('/', function (req, res, next) {
//   //console.log(req)
//   if (req.body) {
//     var lobby_inspetion = {
//       id:req.body.id,
//       insp_list_id: req.body.insp_list_id,
//       abnormality_id: req.body.abnormality_id,
//       location_id: req.body.location_id,
//       lobby_name: req.body.lobby_name,
//       category: req.body.category,
//       name_onduty_crewcontroller: req.body.name_onduty_crewcontroller,
//       combined: req.body.combined,
//       size: req.body.size,
//       ventilation_lighting: req.body.ventilation_lighting,
//       furniture: req.body.furniture,
//       water: req.body.water,
//       coolers: req.body.coolers,
//       power_backup: req.body.power_backup,
//       phone: req.body.phone,
//       attached_restroom: req.body.attached_restroom,
//       lockers: req.body.lockers,
//       registers: req.body.registers,
//       notice_book: req.body.notice_book,
//       track_signal_defect: req.body.track_signal_defect,
//       safety_notice: req.body.safety_notice,
//       speed_board: req.body.speed_board,
//       ba: req.body.ba,
//       cms_ss: req.body.cms_ss,
//       spectacle_board: req.body.spectacle_board,
//       staff_avl_board: req.body.staff_avl_board,
//       suspected_drinkers: req.body.suspected_drinkers,
//       list_pme_due: req.body.list_pme_due,
//       list_ref_due: req.body.list_ref_due,
//       observation: req.body.observation
//     }
//     if (req.body.lobby_inspetion.length > 0) {
//       pool.getConnection(function (err, connection) {
//         if (err) {
//           return done(null);
//         }
//         util.bulkInsert(connection, 'clifb_lobby_inspection', req.body.lobby_inspetion, (err, results) => {
//           if (err) {
//             return res.status(401).send({
//               success: false,
//               msg: err
//             });
//           }
//           return res.json(results);
//         });
//       });
//     } else {
//       return res.status(401).send({
//         success: false,
//         msg: "No inspetion provided"
//       });
//     }

//   }
// })

router.delete('/', function (req, res, next) {
  console.log(req.query)
  if (req.query) {
    pool.query('Delete from clifb_lobby_inspection where id = ?', [req.query.id], function (err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      //  console.log(results);
      return res.json(results);

    });
  }
})
getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = router;
