var express = require('express');
var router = express.Router();
var passport = require('passport');
require('../config/passport')(passport);
var pool = require('../config/mysqlconnpool').pool;
var moment = require('moment');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var settings = require('../config/settings');
require('../config/passport')(passport);

const multer = require('multer');
// const db = require('db');
const fs = require('fs');
// const uuidv1 = require('uuid/v1');
const path = require('path');
/* GET ALL TRAINS */
router.get('/', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT * FROM leave_reg order by srl_no', function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      // console.log(results);
      return res.json(results);
    });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

/* GET ALL TRAINS */
router.get('/emp_id', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  //console.log(req.params.device_id)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT * FROM leave_re where emp_id= ?', [req.query.emp_id], function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      return res.json(results);
    });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

/* GET ALL TRAINS */
router.get('/dnload', function(req, res) {
  //console.log(req.params.device_id)
    pool.query("emp_id,E_yearL,E_month,emp_id,leave_type,sick_type," +
    "DATE_FORMAT(`leave_reg`.`l_from`, '%Y-%m-%d') `l_from`," +
    "DATE_FORMAT(`leave_reg`.`l_to`, '%Y-%m-%d') `l_to`," +
    "no_of_days,remarks,Type,mult,P_LEAVE_TYPE, no_of_leave_taken" +
    "FROM leave_reg",  function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      return res.json(results);
    });
});

router.post('/syncLeave_reg', function (req, res) {
  if (req.body.length > 0) {
   pool.getConnection(function (err, connection) {
     if (err) {
       return res.status(400).send(err)
     }
     util.bulkInsert(connection, 'leave_reg', req.body, (err1, results) => {
       console.log(results)
       if (err1) {
         console.log(err1)
         return res.status(400).send(err1)
       } else {
         return res.json({
           success: true,
           msg: 'Successfully Synchronised leave register.'
         });
       }
     })
   })
 }
});


router.post('/employees_upload', function (req, res) {
  upload(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      console.log(err)
      res.status(401).send({
        success: false,
        msg: err
      });
    } else if (err) {
      console.log(err)
      res.status(401).send({
        success: false,
        msg: err
      });
    } else if (req.file) {
      let keys = []
      let arrayOfobj = []
      let stream = fs.createReadStream("./files/uploads/file.csv");
      let csvData = [];
      let csvStream = fastcsv
        .parse()
        .on("data", function (data) {
          csvData.push(data);
          keys = csvData[0]
        })
        .on("end", function () {
          // remove the first line: header
          csvData.shift();
          if (csvData.length > 0) {
            for (i = 0; i < csvData.length; i++) {
              var obj = {}
              for(j=0; j < keys.length; j++) {
                obj[keys[j]] = csvData[i][j]
              }
               arrayOfobj.push(obj)
            }
            // console.log(arrayOfobj)
            pool.getConnection(function (err, connection) {
              if (err) {
                console.error(err);
                res.status(401).send({
                  success: false,
                  msg: "Error in uploading, try again."
                });
              }
              util.bulkInsert(connection, "leave_reg",arrayOfobj,(err1, results)=> {
                if (err1) {
                  console.log(err1)
                  res.status(401).send({
                    success: false,
                    msg: "Error in uploading, try again."
                  });
                } else {
                  res.status(200).send({
                    success: true,
                    msg: "Succesfully uploaded leave register"
                  });
                }
              })
            })
           }
        });
      stream.pipe(csvStream);
    }
  })
})

getToken = function(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = router;
