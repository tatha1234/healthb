var express = require('express');
var router = express.Router();
var passport = require('passport');
require('../config/passport')(passport);
var pool = require('../config/mysqlconnpool').pool;
var moment = require('moment');
var _ = require('lodash');
var util = require('./../modules/util');

/* GET ALL BOOKS */
router.get('/', function(req, res) {
  // var token = getToken(req.headers);
  // if (token) {
  pool.query(`SELECT * from clifb_location`, function(err, results, fields) {
    if (err) {
      return res.status(401).send({
        success: false,
        msg: err
      });
    }
    return res.json(results);
  });
  // } else {
  //   return res.status(403).send({
  //     success: false,
  //     msg: 'Unauthorized.'
  //   });
  // }
});

/* GET SINGLE BOOK BY ID */
router.get('/id', function(req, res, next) {
  pool.query(`SELECT * from clifb_location where id = ?`, [req.query.id], function(err, results, fields) {
    if (err) {
      return res.status(401).send({
        success: false,
        msg: err
      });
    }
    console.log(results);
    return res.json(results);
  });
});

/* GET SINGLE BOOK BY ID */
router.post('/', function(req, res, next) {
  // console.log(req)
  if (req.body) {
    var location = {
      insp_list_id: req.body.insp_list_id,
      lat: req.body.lat,
      long: req.body.long,
      date_time: req.body.date_time,
      place: req.body.place
     
      
    }
    pool.query('INSERT INTO clifb_location SET ?', location, function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      //  console.log(results);
      return res.json(results);
    });
  }
})

router.put('/', function(req, res, next) {
  // console.log(req)
  if (req.body) {
    var location = {
      id : req.body.id,
      insp_list_id: req.body.insp_list_id,
      lat: req.body.lat,
      long: req.body.long,
      date_time: req.body.date_time,
      place: req.body.place
     
      
    }
    pool.query('UPDATE clifb_location SET ? WHERE id = ?', [location , location.id], function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      //  console.log(results);
      return res.json(results);
    });
  }
})




// router.put('/', function(req, res, next) {
//   //console.log(req)
//   if (req.body) {
//       var location = {
//         insp_list_id: req.body.insp_list_id,
//       lat: req.body.lat,
//       long: req.body.long,
//       date_time: req.body.date_time,
//       place: req.body.place
//     }
//     pool.query('UPDATE clifb_location SET ? where id = ?', [location, req.body.id], function(err, results, fields) {
//       if (err) {
//         return res.status(401).send({
//           success: false,
//           msg: err
//         });
//       }
//       if (req.body.location.length > 0) {
//         pool.getConnection(function(err, connection) {
//           if (err) {
//             return done(null);
//           }
//           util.bulkInsert(connection, 'clifb_location', req.body.location, (err1, results1) => {
//             if (err1) {
//               return res.status(401).send({
//                 success: false,
//                 msg: err1
//               });
//             }
//             //      console.log(final_ctr)
//             return res.json(results1);
//           });
//         });
//       } else {
//         return res.json(results);
//       }
//     });
//   }
// })

router.delete('/', function(req, res, next) {
  console.log(req.query)
  if (req.query) {
    pool.query('Delete from clifb_location where id = ?', [req.query.id], function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
        //  console.log(results);
        return res.json(results);

    });
  }
})
getToken = function(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = router;
