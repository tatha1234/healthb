var express = require('express');
var router = express.Router();
var passport = require('passport');
require('../config/passport')(passport);
var pool = require('../config/mysqlconnpool').pool;
var moment = require('moment');
var _ = require('lodash');
var util = require('./../modules/util');

/* GET ALL BOOKS */
router.get('/', function (req, res) {
  // var token = getToken(req.headers);
  // if (token) {
  pool.query(`SELECT * from clifb_running_rooms`, function (err, results, fields) {
    if (err) {
      return res.status(401).send({
        success: false,
        msg: err
      });
    }
    return res.json(results);
  });
  // } else {
  //   return res.status(403).send({
  //     success: false,
  //     msg: 'Unauthorized.'
  //   });
  // }
});

/* GET SINGLE BOOK BY ID */

router.get('/id', function (req, res, next) {
  pool.query(`SELECT * from clifb_running_rooms where id = ?`, [req.query.id], function (err, results, fields) {
    if (err) {
      return res.status(401).send({
        success: false,
        msg: err
      });
    }
    console.log(results);
    return res.json(results);
  });
});

/* GET SINGLE BOOK BY ID */
router.post('/', function (req, res, next) {
  // console.log(req)
  if (req.body) {
    var rroom_inspetion = {
      
      insp_list_id: req.body.insp_list_id,
      abnormality_id: req.body.abnormality_id,
      location_id: req.body.location_id,
      rrooms_name: req.body.rrooms_name,
      nodal_officer: req.body.nodal_officer,
      no_of_rooms: req.body.no_of_rooms,
      no_of_beds: req.body.no_of_beds,
      water: req.body.water,
      avg_occupancy_day: req.body.avg_occupancy_day,
      name_of_staff: req.body.name_of_staff,
      water: req.body.water,
      toilet: req.body.toilet,
      hygine: req.body.hygine,
      ventilation_lighting: req.body.ventilation_lighting,
      coolers: req.body.coolers,
      reading_room: req.body.reading_room,
      cooking: req.body.cooking,
      medical: req.body.medical,
      bed_per_room: req.body.bed_per_room,
      boundary_wall: req.body.boundary_wall,
      power_back_up: req.body.power_back_up,
      mosquito: req.body.mosquito,
      phone: req.body.phone,
      lockers: req.body.lockers,
      first_aid: req.body.first_aid,
      fire_fighting: req.body.fire_fighting,
      linen: req.body.linen,
      meal: req.body.meal,
      safety_poster: req.body.safety_poster,
      observation: req.body.observation
     }
    pool.query('INSERT INTO clifb_running_rooms SET ?', rroom_inspetion, function (err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      //  console.log(results);
      return res.json(results);
    });
  }
})
router.put('/', function (req, res, next) {
  // console.log(req)
  if (req.body) {
    var rroom_inspetion = {
      id : req.body.id,
      insp_list_id: req.body.insp_list_id,
      abnormality_id: req.body.abnormality_id,
      location_id: req.body.location_id,
      rrooms_name: req.body.rrooms_name,
      nodal_officer: req.body.nodal_officer,
      no_of_rooms: req.body.no_of_rooms,
      no_of_beds: req.body.no_of_beds,
      water: req.body.water,
      avg_occupancy_day: req.body.avg_occupancy_day,
      name_of_staff: req.body.name_of_staff,
      water: req.body.water,
      toilet: req.body.toilet,
      hygine: req.body.hygine,
      ventilation_lighting: req.body.ventilation_lighting,
      coolers: req.body.coolers,
      reading_room: req.body.reading_room,
      cooking: req.body.cooking,
      medical: req.body.medical,
      bed_per_room: req.body.bed_per_room,
      boundary_wall: req.body.boundary_wall,
      power_back_up: req.body.power_back_up,
      mosquito: req.body.mosquito,
      phone: req.body.phone,
      lockers: req.body.lockers,
      first_aid: req.body.first_aid,
      fire_fighting: req.body.fire_fighting,
      linen: req.body.linen,
      meal: req.body.meal,
      safety_poster: req.body.safety_poster,
      observation: req.body.observation
    
    }
    pool.query('UPDATE clifb_running_rooms SET ? WHERE id = ?', [rroom_inspetion , rroom_inspetion.id], function (err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      //  console.log(results);
      return res.json(results);
    });
  }
})


// router.put('/', function (req, res, next) {
//   //console.log(req)
//   if (req.body) {
//     var rroom_inspetion = {
//         insp_list_id: req.body.insp_list_id,
//         abnormality_id: req.body.abnormality_id,
//         location_id: req.body.location_id,
//         rrooms_name: req.body.rrooms_name,
//         nodal_officer: req.body.nodal_officer,
//         no_of_rooms: req.body.no_of_rooms,
//         no_of_beds: req.body.no_of_beds,
//         water: req.body.water,
//         avg_occupancy_day: req.body.avg_occupancy_day,
//         furniture: req.body.furniture,
//         water: req.body.water,
//         toilet: req.body.toilet,
//         hygine: req.body.hygine,
//         ventilation_lighting: req.body.ventilation_lighting,
//         coolers: req.body.coolers,
//         reading_room: req.body.reading_room,
//         cooking: req.body.cooking,
//         medical: req.body.medical,
//         bed_per_room: req.body.bed_per_room,
//         boundary_wall: req.body.boundary_wall,
//         power_back_up: req.body.power_back_up,
//         mosquito: req.body.mosquito,
//         cms_ss: req.body.cms_ss,
//         phone: req.body.phone,
//         lockers: req.body.lockers,
//         first_aid: req.body.first_aid,
//         fire_fighting: req.body.fire_fighting,
//         linen: req.body.linen,
//         meal: req.body.meal,
//         safety_poster: req.body.safety_poster,
//         observation: req.body.observation
      
//     }
//     if (req.body.rroom_inspetion.length > 0) {
//       pool.getConnection(function (err, connection) {
//         if (err) {
//           return done(null);
//         }
//         util.bulkInsert(connection, 'clifb_running_rooms', req.body.rroom_inspetion, (err, results) => {
//           if (err) {
//             return res.status(401).send({
//               success: false,
//               msg: err
//             });
//           }
//           return res.json(results);
//         });
//       });
//     } else {
//       return res.status(401).send({
//         success: false,
//         msg: "No inspetion provided"
//       });
//     }

//   }
// })

// router.delete('/', function (req, res, next) {
//   console.log(req.query)
//   if (req.query) {
//     pool.query('Delete from clifb_running_rooms where id = ?', [req.query.id], function (err, results, fields) {
//       if (err) {
//         return res.status(401).send({
//           success: false,
//           msg: err
//         });
//       }
//       //  console.log(results);
//       return res.json(results);

//     });
//   }
// })
getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = router;
