var express = require('express');
var router = express.Router();
var passport = require('passport');
require('../config/passport')(passport);
var pool = require('../config/mysqlconnpool').pool;
var moment = require('moment');
var _ = require('lodash');
var util = require('./../modules/util');

/* GET ALL BOOKS */
router.get('/', function (req, res) {
  // var token = getToken(req.headers);
  // if (token) {
  pool.query(`SELECT * from clifb_gate_inspection`, function (err, results, fields) {
    if (err) {
      return res.status(401).send({
        success: false,
        msg: err
      });
    }
    return res.json(results);
  });
  // } else {
  //   return res.status(403).send({
  //     success: false,
  //     msg: 'Unauthorized.'
  //   });
  // }
});

/* GET SINGLE BOOK BY ID */
router.get('/id', function (req, res, next) {
  pool.query(`SELECT * from clifb_gate_inspection where id = ?`, [req.query.id], function (err, results, fields) {
    if (err) {
      return res.status(401).send({
        success: false,
        msg: err
      });
    }
    console.log(results);
    return res.json(results);
  });
});

/* GET SINGLE BOOK BY ID */
router.post('/', function (req, res, next) {
  // console.log(req)
  if (req.body) {
    var gate_inspetion = {
      insp_list_id: req.body.insp_list_id,
      abnormality_id: req.body.abnormality_id,
      location_id: req.body.location_id,
      gate_no: req.body.gate_no,
      tvu_no: req.body.tvu_no,
      gate_man: req.body.gate_man,
      desig: req.body.desig,
      pme_due: req.body.pme_due,
      trans_due: req.body.trans_due,
      safety_chain: req.body.safety_chain,
      boom_stop_red_light: req.body.boom_stop_red_light,
      height_gauge: req.body.height_gauge,
      rd_user_warning_board: req.body.rd_user_warning_board,
      street_light: req.body.street_light,
      wl_boad: req.body.wl_boad,
      check_rail: req.body.check_rail,
      visibility_driver: req.body.visibility_driver,
      visibility_users: req.body.visibility_users,
      retro_paint: req.body.retro_paint,
      trilingual_gr: req.body.trilingual_gr,
      surface: req.body.surface,
      buzzer: req.body.buzzer,
      interlocking: req.body.interlocking,
      speed_breaker: req.body.speed_breaker,
      alt_power_supply: req.body.alt_power_supply,
      observation: req.body.observation
    
    }
    pool.query('INSERT INTO clifb_gate_inspection SET ?', gate_inspetion, function (err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      //  console.log(results);
      return res.json(results);
    });
  }
})

router.put('/', function (req, res, next) {
  // console.log(req)
  if (req.body) {
    var gate_inspetion = {
      id : req.body.id,
      insp_list_id: req.body.insp_list_id,
      abnormality_id: req.body.abnormality_id,
      location_id: req.body.location_id,
      gate_no: req.body.gate_no,
      tvu_no: req.body.tvu_no,
      gate_man: req.body.gate_man,
      desig: req.body.desig,
      pme_due: req.body.pme_due,
      trans_due: req.body.trans_due,
      safety_chain: req.body.safety_chain,
      boom_stop_red_light: req.body.boom_stop_red_light,
      height_gauge: req.body.height_gauge,
      rd_user_warning_board: req.body.rd_user_warning_board,
      street_light: req.body.street_light,
      wl_boad: req.body.wl_boad,
      check_rail: req.body.check_rail,
      visibility_driver: req.body.visibility_driver,
      visibility_users: req.body.visibility_users,
      retro_paint: req.body.retro_paint,
      trilingual_gr: req.body.trilingual_gr,
      surface: req.body.surface,
      buzzer: req.body.buzzer,
      interlocking: req.body.interlocking,
      speed_breaker: req.body.speed_breaker,
      alt_power_supply: req.body.alt_power_supply,
      observation: req.body.observation
    
    }
    pool.query('UPDATE clifb_gate_inspection SET ? WHERE id = ?', [gate_inspetion , gate_inspetion.id], function (err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      //  console.log(results);
      return res.json(results);
    });
  }
})






// router.put('/', function (req, res, next) {
//   //console.log(req)
//   if (req.body) {
//     var gate_inspetion = {
//         insp_list_id: req.body.insp_list_id,
//         abnormality_id: req.body.abnormality_id,
//         location_id: req.body.location_id,
//         gate_no: req.body.gate_no,
//         tvu_no: req.body.tvu_no,
//         gate_man: req.body.gate_man,
//         desig: req.body.desig,
//         pme_due: req.body.pme_due,
//         trans_due: req.body.trans_due,
//         safety_chain: req.body.safety_chain,
//         boom_stop_red_light: req.body.boom_stop_red_light,
//         height_gauge: req.body.height_gauge,
//         rd_user_warning_board: req.body.rd_user_warning_board,
//         street_light: req.body.street_light,
//         wl_boad: req.body.wl_boad,
//         check_rail: req.body.check_rail,
//         visibility_driver: req.body.visibility_driver,
//         visibility_users: req.body.visibility_users,
//         retro_paint: req.body.retro_paint,
//         trilingual_gr: req.body.trilingual_gr,
//         surface: req.body.surface,
//         buzzer: req.body.buzzer,
//         interlocking: req.body.interlocking,
//         speed_breaker: req.body.speed_breaker,
//         alt_power_supply: req.body.alt_power_supply,
//         observation: req.body.observation
//     }
//     if (req.body.gate_inspetion.length > 0) {
//       pool.getConnection(function (err, connection) {
//         if (err) {
//           return done(null);
//         }
//         util.bulkInsert(connection, 'clifb_gate_inspection', req.body.gate_inspetion, (err, results) => {
//           if (err) {
//             return res.status(401).send({
//               success: false,
//               msg: err
//             });
//           }
//           return res.json(results);
//         });
//       });
//     } else {
//       return res.status(401).send({
//         success: false,
//         msg: "No inspetion provided"
//       });
//     }

//   }
// })

router.delete('/', function (req, res, next) {
  console.log(req.query)
  if (req.query) {
    pool.query('Delete from clifb_gate_inspection where id = ?', [req.query.id], function (err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      //  console.log(results);
      return res.json(results);

    });
  }
})
getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = router;
