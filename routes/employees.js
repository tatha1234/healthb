var express = require('express');
var router = express.Router();
var passport = require('passport');
require('../config/passport')(passport);
var pool = require('../config/mysqlconnpool').pool;
var moment = require('moment');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var settings = require('../config/settings');
require('../config/passport')(passport);
const querystring = require('querystring');
var util = require('../modules/util')
/* GET ALL TRAINS */
router.get('/', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT (@sno := @sno + 1) as Sno,`employees`.`emp_id`,' + '`employees`.`Name`,' +
    '`employees`.`Present Designation`,' +
    '`employees`.`Working as`,' +
    '`employees`.`Working at`,' +
    '`employees`.`Posting Shed`,' +
    '`employees`.`Gradation`,' +
    '`employees`.`Name of LI or SLI`,' +
    '`employees`.`Qualifications`,' +
    " DATE_FORMAT(`employees`.`Date of Appointment`, '%Y-%m-%d') `Date of Appointment`," +
    " DATE_FORMAT(`employees`.`Date of Birth`, '%Y-%m-%d') `Date of Birth`," +
    '`employees`.`Date of joining in AC Traction`,' +
    '`employees`.`pf no`,' +
    '`employees`.`emp_id`,' +
    '`employees`.`Date of joining as Asst Driver`,' +
    " DATE_FORMAT(`employees`.`Date of Promotion Rgular as ET`, '%Y-%m-%d') `Date of Promotion Rgular as ET`," +
    " DATE_FORMAT(`employees`.`Date of Promotion Rgular as Goods Driver`, '%Y-%m-%d') `Date of Promotion Rgular as Goods Driver`," +
    " DATE_FORMAT(`employees`.`Date of Promotion as Rgular  Pass Driver`, '%Y-%m-%d') `Date of Promotion as Rgular  Pass Driver`," +
    " DATE_FORMAT(`employees`.`Date of Promotion Regular as Spl Grade Driver`, '%Y-%m-%d') `Date of Promotion Regular as Spl Grade Driver`," +
    '`employees`.`Detail of Promotions`,' +
    '`employees`.`Date of attaining 45 years`,' +
    '`employees`.`Date of attaining 55 years`,' +
    '`employees`.`Date of Retirement`,' +
    " DATE_FORMAT(`employees`.`Last PME done on`, '%Y-%m-%d') `Last PME done on`," +
    " DATE_FORMAT(`employees`.`PME due on`, '%Y-%m-%d') `PME due on`," +
    " DATE_FORMAT(`employees`.`PME due on`, '%Y-%m-%d') `PME_due_on`," +
    '`employees`.`Detail of PMEs`,' +
    '`employees`.`With Spectacles`,' +
    '`employees`.`With NV Spectacles`,' +
    " DATE_FORMAT(`employees`.`Last ZTC done on`, '%Y-%m-%d') `Last ZTC done on`," +
    " DATE_FORMAT(`employees`.`ZTC due on`, '%Y-%m-%d') `ZTC due on`," +
    " DATE_FORMAT(`employees`.`ZTC due on`, '%Y-%m-%d') `ZTC_due_on`," +
    " DATE_FORMAT(`employees`.`Last Technical Ref done on`, '%Y-%m-%d') `Last Technical Ref done on`," +
    " DATE_FORMAT(`employees`.`Technical Ref due on`, '%Y-%m-%d') `Technical Ref due on`," +
    " DATE_FORMAT(`employees`.`Technical Ref due on`, '%Y-%m-%d') `Technical_Ref_due_on`," +
    '`employees`.`Detail ofTechnical Refresher`,' +
    " DATE_FORMAT(`employees`.`Last Safety Camp or seminar attended on`, '%Y-%m-%d') `Last Safety Camp or seminar attended on`," +
    '`employees`.`Detail of Safety Camp or seminar`,' +
    '`employees`.`Present Address`,' +
    '`employees`.`Present City`,' +
    '`employees`.`Present District`,' +
    '`employees`.`Present P O`,' +
    '`employees`.`Present PIN`,' +
    '`employees`.`Present Phone No`,' +
    '`employees`.`Present State`,' +
    '`employees`.`Permanent Address`,' +
    '`employees`.`Permanent City`,' +
    '`employees`.`Permanent District`,' +
    '`employees`.`Permanent P O`,' +
    '`employees`.`Permanent PIN`,' +
    '`employees`.`Permanent Phone No`,' +
    '`employees`.`Permanent State`,' +
    '`employees`.`Punishment`,' +
    '`employees`.`Rewards & acheivements`,' +
    '`employees`.`Status`,' +
    '`employees`.`Other informations`,' +
    '`employees`.`Q_TYPE`,' +
    '`employees`.`AGE_GROUP`,' +
    '`employees`.`WORKING IN`,' +
    '`employees`.`PASS_SRL`,' +
    '`employees`.`DESIG_SRL`,' +
    '`employees`.`FULL NAME`,' +
    '`employees`.`Uniform Received`,' +
    '`employees`.`Automatic Cert Due on`,' +
    '`employees`.`seniority_pos`,' +
    '`employees`.`CUG_NO`,' +
    '`employees`.`BILL_UNIT`,' +
    '`employees`.`CATEGORY`,' +
    '`employees`.`PSY_ATTND`,' +
    '`employees`.`PSY_DATE`,' +
    '`employees`.`PSY_RESULT`,' +
    '`employees`.`PSY_DETAIL`,' +
    '`employees`.`no_of_app_inst`,' +
    '`desig_srl`.`C_NEW_DESIG_SHORTEST` as Designation,' +
      "`employees`.`Detail of ZTC` FROM `employees` left join `supervisors` on `employees`.`Name of LI or SLI` = `supervisors`.`emp_id` left join desig_srl on `employees`.`Present Designation` = desig_srl.DESIG, (SELECT @sno := 0) as ini where status = 'WORKING' order by `Designation`,`Posting Shed`,Name",
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  // } else {
  //   return res.status(403).send({
  //     success: false,
  //     msg: 'Unauthorized.'
  //   });
  // }
    }
});



/* GET ALL TRAINS */
router.get('/emp_id', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  //console.log(req.params.device_id)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT * FROM employees where emp_id= ?', [req.query.id], function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      return res.json(results);
    });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

/* Get loco info from loco table*/
router.get('/by_shed', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT `employees`.`Name`,' +
      '`employees`.`Present Designation`,' +
      '`desig_srl`.`C_NEW_DESIG_SHORTEST` as Designation,' +
      '`employees`.`Posting Shed`,' +
      '`employees`.`Gradation`,' +
      '`employees`.`pf no`,' +
      '`employees`.`CUG_NO`,' +
      '`employees`.`Name of LI or SLI`,' +
      '`employees`.`emp_id`,' +
      '`employees`.`Status`,' +
      '`supervisors`.`Name` as CLI ' +
      "FROM employees  left join `supervisors` on `employees`.`Name of LI or SLI` = `supervisors`.`emp_id` left join desig_srl on `employees`.`Present Designation` = desig_srl.DESIG, (SELECT @sno := 0) as ini where status = 'WORKING' and `employees`.`Posting Shed` = ? order by name",
      [req.query.shed],
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

/* Get loco info from loco table*/
router.get('/addressmap', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    var shed_clause = ''
    var desig_clause = ''
    if (req.query.shed != 'All') {
      shed_clause = " and `employees`.`Posting Shed` = '" + req.query.shed + "'"
    }
    if (req.query.desig != 'All') {
      desig_clause = " and `desig_srl`.`C_NEW_DESIG_SHORTEST` IN (" + req.query.desig + ")"
    }
    console.log(req.query,shed_clause,desig_clause)
    pool.query('SELECT `employees`.`Name`,' +
      '`employees`.`Present Designation`,' +
      '`desig_srl`.`C_NEW_DESIG_SHORTEST` as Designation,' +
      '`employees`.`Posting Shed`,' +
      '`employees`.`Gradation`,' +
      '`employees`.`pf no`,' +
      '`employees`.`CUG_NO`,' +
      '`employees`.`Name of LI or SLI`,' +
      '`employees`.`emp_id`,' +
      '`employees`.`Status`,' +
      '`employees`.`Present Address`,' +
      '`employees`.`Present City`,' +
      '`employees`.`Present District`,' +
      '`employees`.`Present P O`,' +
      '`employees`.`Present PIN`,' +
      '`employees`.`Present Phone No`,' +
      '`employees`.`Present State`,' +
      '`supervisors`.`Name` as CLI,' +
      '`pincode_lat_lang`.`pin_code`,'+
      '`pincode_lat_lang`.`post_office`,'+
      '`pincode_lat_lang`.`state`,'+
      '`pincode_lat_lang`.`latitude`,'+
      '`pincode_lat_lang`.`longitude` '+
      "FROM employees  left join `supervisors` on `employees`.`Name of LI or SLI` = `supervisors`.`emp_id` "+
      "left join desig_srl on `employees`.`Present Designation` = desig_srl.DESIG "+
      "left join `pincode_lat_lang` on `employees`.`Present PIN` = `pincode_lat_lang`.`pin_code` "+
      "where status = 'WORKING' and `employees`.`Present PIN` is not null " + shed_clause + desig_clause,
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

/* Get loco info from loco table*/
router.get('/sup_id', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT (@sno := @sno + 1) as Sno,`employees`.`Name`,' +
    '`employees`.`Present Designation`,' +
    '`employees`.`Working as`,' +
    '`employees`.`Working at`,' +
    '`employees`.`Posting Shed`,' +
    '`employees`.`Gradation`,' +
    '`employees`.`Name of LI or SLI`,' +
    '`employees`.`Qualifications`,' +
    " DATE_FORMAT(`employees`.`Date of Appointment`, '%Y-%m-%d') `Date of Appointment`," +
    " DATE_FORMAT(`employees`.`Date of Birth`, '%Y-%m-%d') `Date of Birth`," +
    '`employees`.`Date of joining in AC Traction`,' +
    '`employees`.`pf no`,' +
    '`employees`.`emp_id`,' +
    '`employees`.`Date of joining as Asst Driver`,' +
    " DATE_FORMAT(`employees`.`Date of Promotion Rgular as ET`, '%Y-%m-%d') `Date of Promotion Rgular as ET`," +
    " DATE_FORMAT(`employees`.`Date of Promotion Rgular as Goods Driver`, '%Y-%m-%d') `Date of Promotion Rgular as Goods Driver`," +
    " DATE_FORMAT(`employees`.`Date of Promotion as Rgular  Pass Driver`, '%Y-%m-%d') `Date of Promotion as Rgular  Pass Driver`," +
    " DATE_FORMAT(`employees`.`Date of Promotion Regular as Spl Grade Driver`, '%Y-%m-%d') `Date of Promotion Regular as Spl Grade Driver`," +
    '`employees`.`Detail of Promotions`,' +
    '`employees`.`Date of attaining 45 years`,' +
    '`employees`.`Date of attaining 55 years`,' +
    '`employees`.`Date of Retirement`,' +
    " DATE_FORMAT(`employees`.`Last PME done on`, '%Y-%m-%d') `Last PME done on`," +
    " DATE_FORMAT(`employees`.`PME due on`, '%Y-%m-%d') `PME due on`," +
    " DATE_FORMAT(`employees`.`PME due on`, '%Y-%m-%d') `PME_due_on`," +
    '`employees`.`Detail of PMEs`,' +
    '`employees`.`With Spectacles`,' +
    '`employees`.`With NV Spectacles`,' +
    " DATE_FORMAT(`employees`.`Last ZTC done on`, '%Y-%m-%d') `Last ZTC done on`," +
    " DATE_FORMAT(`employees`.`ZTC due on`, '%Y-%m-%d') `ZTC due on`," +
    " DATE_FORMAT(`employees`.`ZTC due on`, '%Y-%m-%d') `ZTC_due_on`," +
    " DATE_FORMAT(`employees`.`Last Technical Ref done on`, '%Y-%m-%d') `Last Technical Ref done on`," +
    " DATE_FORMAT(`employees`.`Technical Ref due on`, '%Y-%m-%d') `Technical Ref due on`," +
    " DATE_FORMAT(`employees`.`Technical Ref due on`, '%Y-%m-%d') `Technical_Ref_due_on`," +
    '`employees`.`Detail ofTechnical Refresher`,' +
    " DATE_FORMAT(`employees`.`Last Safety Camp or seminar attended on`, '%Y-%m-%d') `Last Safety Camp or seminar attended on`," +
    '`employees`.`Detail of Safety Camp or seminar`,' +
    '`employees`.`Present Address`,' +
    '`employees`.`Present City`,' +
    '`employees`.`Present District`,' +
    '`employees`.`Present P O`,' +
    '`employees`.`Present PIN`,' +
    '`employees`.`Present Phone No`,' +
    '`employees`.`Present State`,' +
    '`employees`.`Permanent Address`,' +
    '`employees`.`Permanent City`,' +
    '`employees`.`Permanent District`,' +
    '`employees`.`Permanent P O`,' +
    '`employees`.`Permanent PIN`,' +
    '`employees`.`Permanent Phone No`,' +
    '`employees`.`Permanent State`,' +
    '`employees`.`Punishment`,' +
    '`employees`.`Rewards & acheivements`,' +
    '`employees`.`Status`,' +
    '`employees`.`Other informations`,' +
    '`employees`.`Q_TYPE`,' +
    '`employees`.`AGE_GROUP`,' +
    '`employees`.`WORKING IN`,' +
    '`employees`.`PASS_SRL`,' +
    '`employees`.`DESIG_SRL`,' +
    '`employees`.`FULL NAME`,' +
    '`employees`.`Uniform Received`,' +
    '`employees`.`Automatic Cert Due on`,' +
    '`employees`.`seniority_pos`,' +
    '`employees`.`CUG_NO`,' +
    '`employees`.`BILL_UNIT`,' +
    '`employees`.`CATEGORY`,' +
    '`employees`.`PSY_ATTND`,' +
    '`employees`.`PSY_DATE`,' +
    '`employees`.`PSY_RESULT`,' +
    '`employees`.`PSY_DETAIL`,' +
    '`employees`.`no_of_app_inst`,' +
    '`desig_srl`.`C_NEW_DESIG_SHORTEST` as Designation,' +
      "health_status.health_status,health_status.health_description,health_status.no_of_app_inst FROM employees left join health_status on `employees`.`emp_id` = health_status.emp_id and health_status.status_date= ?  left join desig_srl on `employees`.`Present Designation` = desig_srl.DESIG, (SELECT @sno := 0) as ini where `Name of LI or SLI`= ? and status = 'WORKING' order by `Designation`,`Posting Shed`,Name",
      [req.query.status_date, req.query.sup_id],
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
       
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});
router.get('/Posting_Shed', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT (@sno := @sno + 1) as Sno,`employees`.`Name`,' +
      '`employees`.`Present Designation`,' +
      '`desig_srl`.`C_NEW_DESIG_SHORTEST` as Designation,' +
      '`employees`.`pf no`,' +
      '`employees`.`CUG_NO`,' +
      '`employees`.`Posting Shed`,' +
      "DATE_FORMAT(`employees`.`PME due on`, '%d/%m/%Y') `PME due on`," +
      "DATE_FORMAT(`employees`.`ZTC due on`, '%d/%m/%Y') `ZTC due on`," +
      "DATE_FORMAT(`employees`.`Technical Ref due on`, '%d/%m/%Y') `Technical Ref due on`," +
      "DATE_FORMAT(`employees`.`Last Safety Camp or seminar attended on`, '%d/%m/%Y') `Last Safety Camp or seminar attended on`," +
      "health_status.no_of_app_inst FROM employees left join health_status on `employees`.`emp_id` = health_status.emp_id and health_status.status_date= ?  left join desig_srl on `employees`.`Present Designation` = desig_srl.DESIG, (SELECT @sno := 0) as ini where `Posting Shed`= ? && STATUS = 'WORKING' and `employees`.`Present Designation` not in ('CLI','TW Driver') && `employees`.`PME due on`<= CURRENT_DATE()",

      [req.query['PME due on'], req.query.Posting_Shed],
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

router.get('/Posting_Shed2', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT (@sno := @sno + 1) as Sno,`employees`.`Name`,' +
      '`employees`.`Present Designation`,' +
      '`desig_srl`.`C_NEW_DESIG_SHORTEST` as Designation,' +
      '`employees`.`pf no`,' +
      '`employees`.`CUG_NO`,' +
      '`employees`.`Posting Shed`,' +

      "DATE_FORMAT(`employees`.`PME due on`, '%d/%m/%Y') `PME due on`," +
      "DATE_FORMAT(`employees`.`ZTC due on`, '%d/%m/%Y') `ZTC due on`," +
      "DATE_FORMAT(`employees`.`Technical Ref due on`, '%d/%m/%Y') `Technical Ref due on`," +
      "DATE_FORMAT(`employees`.`Last Safety Camp or seminar attended on`, '%d/%m/%Y') `Last Safety Camp or seminar attended on`," +

      "health_status.no_of_app_inst FROM employees left join health_status on `employees`.`emp_id` = health_status.emp_id and health_status.status_date= ?  left join desig_srl on `employees`.`Present Designation` = desig_srl.DESIG, (SELECT @sno := 0) as ini where `Posting Shed`= ? && STATUS = 'WORKING' and `employees`.`Present Designation` not in ('CLI','TW Driver') && `employees`.`Technical Ref due on`<= CURRENT_DATE() ",

      [req.query['PME due on'], req.query.Posting_Shed],
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});
router.get('/Posting_Shed3', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT (@sno := @sno + 1) as Sno,`employees`.`Name`,' +
      '`employees`.`Present Designation`,' +
      '`desig_srl`.`C_NEW_DESIG_SHORTEST` as Designation,' +
      '`employees`.`pf no`,' +
      '`employees`.`CUG_NO`,' +
      '`employees`.`Posting Shed`,' +

      "DATE_FORMAT(`employees`.`PME due on`, '%d/%m/%Y') `PME due on`," +
      "DATE_FORMAT(`employees`.`ZTC due on`, '%d/%m/%Y') `ZTC due on`," +
      "DATE_FORMAT(`employees`.`Technical Ref due on`, '%d/%m/%Y') `Technical Ref due on`," +
      "DATE_FORMAT(`employees`.`Last Safety Camp or seminar attended on`, '%d/%m/%Y') `Last Safety Camp or seminar attended on`," +

      "health_status.no_of_app_inst FROM employees left join health_status on `employees`.`emp_id` = health_status.emp_id and health_status.status_date= ?  left join desig_srl on `employees`.`Present Designation` = desig_srl.DESIG, (SELECT @sno := 0) as ini where `Posting Shed`= ? && STATUS = 'WORKING' and `employees`.`Present Designation` not in ('CLI','TW Driver') && `employees`.`ZTC due on`<= CURRENT_DATE()",

      [req.query['PME due on'], req.query.Posting_Shed],
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});
router.get('/Posting_Shed4', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT (@sno := @sno + 1) as Sno,`employees`.`Name`,' +
      '`employees`.`Present Designation`,' +
      '`desig_srl`.`C_NEW_DESIG_SHORTEST` as Designation,' +
      '`employees`.`pf no`,' +
      '`employees`.`CUG_NO`,' +
      '`employees`.`Posting Shed`,' +

      "DATE_FORMAT(`employees`.`PME due on`, '%d/%m/%Y') `PME due on`," +
      "DATE_FORMAT(`employees`.`ZTC due on`, '%d/%m/%Y') `ZTC due on`," +
      "DATE_FORMAT(`employees`.`Technical Ref due on`, '%d/%m/%Y') `Technical Ref due on`," +
      "DATE_FORMAT(`employees`.`Last Safety Camp or seminar attended on`, '%d/%m/%Y') `Last Safety Camp or seminar attended on`," +

      "health_status.no_of_app_inst FROM employees left join health_status on `employees`.`emp_id` = health_status.emp_id and health_status.status_date= ?  left join desig_srl on `employees`.`Present Designation` = desig_srl.DESIG, (SELECT @sno := 0) as ini where `Posting Shed`= ? && STATUS = 'WORKING' and `employees`.`Present Designation` not in ('CLI','TW Driver') && `employees`.`PME due on`<= CURRENT_DATE() && `employees`.`ZTC due on`<= CURRENT_DATE()&& `employees`.`Technical Ref due on`<= CURRENT_DATE()",

      [req.query['PME due on'], req.query.Posting_Shed],
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

/* Get loco info from loco table*/
router.get('/cli_dist', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT  (@sno := @sno + 1) as Sno,`supervisors`.`Name`,`supervisors`.`emp_id`,' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("MAIL DRIVER")  THEN 1 ELSE 0 END) AS "LPM",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("PASS DRIVER","SR. PASS DRIVER")  THEN 1 ELSE 0 END) AS "LPP",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("MOTORMAN")  THEN 1 ELSE 0 END) AS "MOTORMAN",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("GOODS DRIVER","SR. GOODS DRIVER")  THEN 1 ELSE 0 END) AS "LPG",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("ET","SR. ET")  THEN 1 ELSE 0 END) AS "LPS",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("ASST. DRIVER","SR. ASST. DRIVER")  THEN 1 ELSE 0 END) AS "ALP",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("TW Driver")  THEN 1 ELSE 0 END) AS "TW Driver",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN' +
      '("MAIL DRIVER","PASS DRIVER","SR. PASS DRIVER","GOODS DRIVER","SR. GOODS DRIVER","ET","SR. ET","ASST. DRIVER","SR. ASST. DRIVER","TW Driver","MOTORMAN")' +
      ' THEN 0 ELSE 1 END) AS "Other",' +
      'SUM( 1 ) AS "TOTAL",' +
      'SUM(CASE WHEN `employees`.`health_status` is null THEN 0 ELSE 1 END) AS "Health Status",' +
      'SUM(`employees`.`no_of_app_inst`) AS "No of App",' +
      'SUM(CASE WHEN `employees`.`Present PIN` is null THEN 0 ELSE 1 END) AS "Address" ' +
      'FROM ' +
      '  employees left join supervisors on `employees`.`Name of LI or SLI` = supervisors.emp_id, (SELECT @sno := 0) as ini' +
      " WHERE STATUS = 'WORKING'  and `employees`.`Present Designation` not in ('CLI','ACC(R)','CC(R)')" +
      ' GROUP BY `supervisors`.`Name`,`supervisors`.`emp_id` order by `supervisors`.`Name`',
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

/* Get loco info from loco table*/
router.get('/staff_position', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT `employees`.`Posting Shed`,' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("MAIL DRIVER")   THEN 1 ELSE 0 END) AS "LPM",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("PASS DRIVER","SR. PASS DRIVER")  THEN 1 ELSE 0 END) AS "LPP",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("MOTORMAN")  THEN 1 ELSE 0 END) AS "MOTORMAN",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("GOODS DRIVER","SR. GOODS DRIVER")  THEN 1 ELSE 0 END) AS "LPG",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("ET","SR. ET")  THEN 1 ELSE 0 END) AS "LPS",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("ASST. DRIVER","SR. ASST. DRIVER")  THEN 1 ELSE 0 END) AS "ALP",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("CC(R)","ACC(R)","PC") THEN 1 ELSE 0 END) AS "Supv",' +
      'SUM( 1 ) AS "TOTAL" ' +
      'FROM ' +
      "employees left join desig_srl on `employees`.`Present Designation` = desig_srl.DESIG WHERE STATUS = 'WORKING' and `employees`.`Present Designation` not in ('CLI','TW Driver') " +
      'GROUP BY `employees`.`Posting Shed`',
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});


router.get('/all_due', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    var where_clause = ''
    if (req.query.start_date && req.query.end_date) {
      where_clause = "`" + req.query.due_type + "` >= '" + req.query.start_date + "' and `" + req.query.due_type +"` <= '" + req.query.end_date + "'"
    } else if (req.query.start_date) {
      where_clause =  "`" + req.query.due_type + "` <= '" + req.query.start_date + "' or `" + req.query.due_type +"` is null"
    }
    console.log(req.query,where_clause)
    pool.query('SELECT `employees`.`Posting Shed`,' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("MAIL DRIVER")   THEN 1 ELSE 0 END) AS "LPM",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("PASS DRIVER","SR. PASS DRIVER")  THEN 1 ELSE 0 END) AS "LPP",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("MOTORMAN")  THEN 1 ELSE 0 END) AS "MOTORMAN",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("GOODS DRIVER","SR. GOODS DRIVER")  THEN 1 ELSE 0 END) AS "LPG",' +
      'SUM(CASE WHEN `employees`.`Present Designation` IN ("ET","SR. ET")  THEN 1 ELSE 0 END) AS "LPS",' +
      'SUM(CASE WHEN `employees`.`Present Designation` IN ("ASST. DRIVER","SR. ASST. DRIVER")  THEN 1 ELSE 0 END) AS "ALP",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("CC(R)","ACC(R)","PC") THEN 1 ELSE 0 END) AS "Supv",' +
      'SUM( 1 ) AS "TOTAL" ' +
      'FROM ' +
      "employees left join desig_srl on `employees`.`Present Designation` = desig_srl.DESIG " +
      "WHERE STATUS = 'WORKING' and `employees`.`Present Designation` not in ('CLI','TW Driver')" + ` and ${where_clause} ` +
      'GROUP BY `employees`.`Posting Shed`',
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

router.get('/pme_due', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT `employees`.`Posting Shed`,' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("MAIL DRIVER")   THEN 1 ELSE 0 END) AS "LPM",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("PASS DRIVER","SR. PASS DRIVER")  THEN 1 ELSE 0 END) AS "LPP",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("MOTORMAN")  THEN 1 ELSE 0 END) AS "MOTORMAN",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("GOODS DRIVER","SR. GOODS DRIVER")  THEN 1 ELSE 0 END) AS "LPG",' +
      'SUM(CASE WHEN `employees`.`Present Designation` IN ("ET","SR. ET")  THEN 1 ELSE 0 END) AS "LPS",' +
      'SUM(CASE WHEN `employees`.`Present Designation` IN ("ASST. DRIVER","SR. ASST. DRIVER")  THEN 1 ELSE 0 END) AS "ALP",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("CC(R)","ACC(R)","PC") THEN 1 ELSE 0 END) AS "Supv",' +
      'SUM( 1 ) AS "TOTAL" ' +
      'FROM ' +
      "employees left join desig_srl on `employees`.`Present Designation` = desig_srl.DESIG WHERE STATUS = 'WORKING' and `employees`.`Present Designation` not in ('CLI','TW Driver') && `employees`.`PME due on`<= CURRENT_DATE()" +
      'GROUP BY `employees`.`Posting Shed`',
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

router.get('/refresher_due', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT `employees`.`Posting Shed`,' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("MAIL DRIVER")   THEN 1 ELSE 0 END) AS "LPM",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("PASS DRIVER","SR. PASS DRIVER")  THEN 1 ELSE 0 END) AS "LPP",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("MOTORMAN")  THEN 1 ELSE 0 END) AS "MOTORMAN",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("GOODS DRIVER","SR. GOODS DRIVER")  THEN 1 ELSE 0 END) AS "LPG",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("ET","SR. ET")  THEN 1 ELSE 0 END) AS "LPS",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("ASST. DRIVER","SR. ASST. DRIVER")  THEN 1 ELSE 0 END) AS "ALP",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("CC(R)","ACC(R)","PC") THEN 1 ELSE 0 END) AS "Supv",' +
      'SUM( 1 ) AS "TOTAL" ' +
      'FROM ' +
      "employees left join desig_srl on `employees`.`Present Designation` = desig_srl.DESIG WHERE STATUS = 'WORKING' and `employees`.`Present Designation` not in ('CLI','TW Driver') && `employees`.`Technical Ref due on`<= CURRENT_DATE()" +
      'GROUP BY `employees`.`Posting Shed`',
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

router.get('/refreshertrans_due', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT `employees`.`Posting Shed`,' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("MAIL DRIVER")   THEN 1 ELSE 0 END) AS "LPM",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("PASS DRIVER","SR. PASS DRIVER")  THEN 1 ELSE 0 END) AS "LPP",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("MOTORMAN")  THEN 1 ELSE 0 END) AS "MOTORMAN",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("GOODS DRIVER","SR. GOODS DRIVER")  THEN 1 ELSE 0 END) AS "LPG",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("ET","SR. ET")  THEN 1 ELSE 0 END) AS "LPS",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("ASST. DRIVER","SR. ASST. DRIVER")  THEN 1 ELSE 0 END) AS "ALP",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("CC(R)","ACC(R)","PC") THEN 1 ELSE 0 END) AS "Supv",' +
      'SUM( 1 ) AS "TOTAL" ' +
      'FROM ' +
      "employees left join desig_srl on `employees`.`Present Designation` = desig_srl.DESIG WHERE STATUS = 'WORKING' and `employees`.`Present Designation` not in ('CLI','TW Driver') && `employees`.`ZTC due on`<= CURRENT_DATE()" +
      'GROUP BY `employees`.`Posting Shed`',
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});








/* Get loco info from loco table*/
router.get('/Sup_health_status', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT `supervisors`.`Name`,`supervisors`.`emp_id`,' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("MAIL DRIVER")  THEN 1 ELSE 0 END) AS "LPM",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("PASS DRIVER","SR. PASS DRIVER")  THEN 1 ELSE 0 END) AS "LPP",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("GOODS DRIVER","SR. GOODS DRIVER")  THEN 1 ELSE 0 END) AS "LPG",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("ET","SR. ET")  THEN 1 ELSE 0 END) AS "LPS",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("ASST. DRIVER","SR. ASST. DRIVER")  THEN 1 ELSE 0 END) AS "ALP",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN ("TW Driver")  THEN 1 ELSE 0 END) AS "TW Driver",' +
      'SUM(CASE WHEN  `employees`.`Present Designation` IN' +
      '("MAIL DRIVER","PASS DRIVER","SR. PASS DRIVER","GOODS DRIVER","SR. GOODS DRIVER","ET","SR. ET","ASST. DRIVER","SR. ASST. DRIVER","TW Driver")' +
      ' THEN 0 ELSE 1 END) AS "Other",' +
      'SUM( 1 ) AS "TOTAL",' +
      'SUM(CASE WHEN `health_status`.`health_status` is null THEN 0 ELSE 1 END) AS "Health Status",' +
      'SUM(`health_status`.`no_of_app_inst`) AS "No of App" ' +
      'FROM ' +
      '  employees left join supervisors on `employees`.`Name of LI or SLI` = supervisors.emp_id left join health_status on `employees`.`emp_id` = health_status.emp_id and  health_status.status_date= ?' +
      " WHERE STATUS = 'WORKING' " +
      ' GROUP BY `supervisors`.`Name`,`supervisors`.`emp_id`', [req.query.status_date],
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});



/* Get loco info from loco table*/
router.get('/health_status', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query("SELECT max(`health_status`.`status_date`) as 'status_date'," +
      "SUM(CASE WHEN  employees.status='WORKING' and `health_status`.`health_status` IN ('ok')  THEN 1 ELSE 0 END) AS 'ok'," +
      "SUM(CASE WHEN  employees.status='WORKING' and `health_status`.`health_status` IN ('not_ok')  THEN 1 ELSE 0 END) AS 'not_ok'," +
      "SUM(CASE WHEN  employees.status='WORKING' and `health_status`.`health_status` IS NULL  THEN 1 ELSE 0 END) AS 'not_reported'," +
      "SUM(CASE WHEN  employees.status='WORKING' THEN `health_status`.`no_of_app_inst` ELSE 0 END) AS 'no_of_app_inst'," +
      "SUM(case when employees.status='WORKING' then 1 else 0 end ) AS 'total' " +
      'FROM ' +
      "employees left join health_status on employees.emp_id = health_status.emp_id and employees.status='WORKING' and status_date = ?", [req.query.status_date],
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

/* GET ALL TRAINS */
router.get('/not_ok_not_reported', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT `employees`.`Name`,' +
      '`employees`.`Present Designation`,' +
      '`employees`.`Posting Shed`,' +
      '`employees`.`pf no`,`employees`.`CUG_NO`,`supervisors`.`Name` as CLI, health_status.health_status, health_status.health_description,' +
      '`employees`.`emp_id`,' +
      '`employees`.`Status` ' +
      "FROM`employees` left join `supervisors` on `employees`.`Name of LI or SLI` = `supervisors`.`emp_id` left join health_status on `employees`.`emp_id` = health_status.emp_id where (health_status.health_status ='not_ok' and status_date = ?) or (status = 'WORKING' and health_status.health_status is null) order by employees.Name", [req.query.status_date],
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});


/* GET UP TRAINS AGAINST DN TRAIN*/
router.get('/employee/:emp_id', passport.authenticate('jwt', {
  session: false
}), function(req, res) {

  var token = getToken(req.headers);
  if (token) {
    pool.query(`SELECT * employees where emp_id = ?`,
      [req.params.emp_id],
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});



/* Update healh_status */
router.put('/update_cli', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req.body)
  // console.log('inside onr post ', req.body);
  var token = getToken(req.headers);
  if (token) {
    jwt.verify(token, settings.secret, (err, decoded) => {
      if (err) {
        return res.json({
          success: false,
          message: 'Token is not valid'
        });
      }

      // if (!req.body.cli_id) {
      //   return res.json({
      //     success: false,
      //     msg: 'Please assign CLI'
      //   });
      // }
      var emp = {
        CUG_NO: req.body.CUG_NO,
        'Name of LI or SLI': req.body.cli_id,
        'Present Designation': req.body['Present Designation'],
        'Posting Shed': req.body['Posting Shed'],
        Status: req.body.Status
      }
      pool.query('Update employees SET ? WHERE emp_id = ?', [emp, req.body.emp_id],
        function(err, results, fields) {
          if (err) {
            return res.status(401).send({
              success: false,
              msg: err
            });
          }
          return res.json({
            success: true,
            msg: 'Successfully updated CLI.',
          });
        });
    });
  } else {
    return res.status(401).send({
      success: false,
      msg: 'unautorised'
    });
  }
});
/* Update healh_status */
router.put('/update_health', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req.body)
  // console.log('inside onr post ', req.body);
  var token = getToken(req.headers);
  if (token) {
    jwt.verify(token, settings.secret, (err, decoded) => {
      if (err) {
        return res.status(401).send({
          success: false,
          message: 'Token is not valid'
        });
      }

      if (!req.body.CUG_NO) {
        return res.status(401).send({
          success: false,
          msg: 'Please pass phone no.'
        });
      }
      pool.getConnection(function(err, connection) {
        if (err) {
          if (connection) {
            connection.release()
          }
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        var emp = {
          CUG_NO: req.body.CUG_NO,
          health_status: req.body.health_status,
          health_description: req.body.health_description,
          no_of_app_inst: req.body.no_of_app_inst
        }
        console.log(req.body.emp_id)
        connection.beginTransaction(function(err) {
          var query = connection.query('Update employees SET ? WHERE emp_id = ?', [emp, req.body.emp_id], function(err, results1, fields) {
            //  console.log(results1, err, fields)
            // if (err) {
            //   connection.rollback(function() {
            //     if (pool._freeConnections.indexOf(connection) {
            //       connection.release()
            //     }
            //     return res.status(401).send({
            //       success: false,
            //       msg: err
            //     });
            //   });
            // }

            var query = connection.query(`INSERT INTO logbook (username,user_type,document_id,event_type)
           values  ('${decoded.username}','${decoded.user_type}',${req.body.emp_id},'Update Health Status')`, function(err, results2, fields) {
              // if (err) {
              //   connection.rollback(function() {
              //     if (pool._freeConnections.indexOf(connection) {
              //       connection.release()
              //     }
              //     return res.status(401).send({
              //       success: false,
              //       msg: err
              //     });
              //   });
              // }
              var query = connection.query(`INSERT INTO health_status (status_date,cli_id,emp_id,health_status,health_description,created_by,no_of_app_inst)
                 VALUES ('${req.body.status_date}',${req.body.cli_id},${req.body.emp_id},'${req.body.health_status}','${req.body.health_description}',
                    '${decoded.username}',${req.body.no_of_app_inst}) ON DUPLICATE KEY UPDATE health_status='${req.body.health_status}',
                     health_description='${req.body.health_description}',no_of_app_inst=${req.body.no_of_app_inst},
                     updated_by = '${decoded.username}'`,
                function(err, results3, fields) {
                  if (err) {
                    connection.rollback(function() {
                      if (pool._freeConnections.indexOf(connection) < 0) {
                        connection.release()
                      }
                      return res.status(401).send({
                        success: false,
                        msg: err
                      });
                    });
                  } else {
                    connection.commit(function(err) {
                      if (err) {
                        connection.rollback(function() {
                          if (pool._freeConnections.indexOf(connection) < 0) {
                            connection.release()
                          }
                          return res.status(401).send({
                            success: false,
                            msg: err
                          });
                        });
                      }
                      if (pool._freeConnections.indexOf(connection) < 0) {
                        connection.release()
                      }
                      return res.json({
                        success: true,
                        msg: 'Successfully update health status.',
                      });
                    });
                  }
                });
            });
          });
        });
      });
    });
  } else {
    return res.status(401).send({
      success: false,
      msg: 'unautorised'
    });
  }
});

router.post('/', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    jwt.verify(token, settings.secret, (err, decoded) => {
      if (err) {
        return res.json({
          success: false,
          message: 'Token is not valid'
        });
      } else {


        if (!req.body.Name) {
          return res.json({
            success: false,
            msg: 'Please pass Name'
          });
        }
        pool.query('SELECT * FROM employees WHERE emp_id = ?',
          [req.body.emp_id],
          function(err, results, fields) {
            if (err) {
              return res.status(401).send({
                success: false,
                msg: err
              });
            }


            if (typeof results !== 'undefined' && results.length > 0) {
              return res.status(401).send({
                success: false,
                msg: 'emp id already added.'
              });
            }
            var onrcase = {
              Name: req.body.Name,
              'Present Designation': req.body['Present Designation'],
              'Posting Shed': req.body['Posting Shed'],
              Qualifications: req.body.Qualifications,
              'Date of Appointment': req.body['Date of Appointment'],
              'Date of Birth': req.body['Date of Birth'],
              'pf no': req.body['pf no'],
              CUG_NO: req.body.cUG_NO,
              Status: req.body.Status,
            }
            var query = pool.query('INSERT INTO employees SET ?', onrcase, function(err, results, fields) {
              if (err) {
                return res.status(401).send({
                  success: false,
                  msg: err
                });
              }
              // var query = pool.query('INSERT INTO employees SET ?', onrcase, function(err, results, fields) {
              //   if (err) {
              //     return res.status(401).send({
              //       success: false,
              //       msg: err
              //     });
              //   }
              var query = pool.query(`INSERT INTO logbook (username,user_type,emp_id,event_type)
                   values  ('${decoded.username}','${decoded.user_type}',${results.insertId},'Create' )`, function(err, results1, fields) {
                if (err) {
                  return res.status(401).send({
                    success: false,
                    msg: err
                  });
                }

                return res.json({
                  success: true,
                  msg: 'Successfully added new employee and log',
                  results: results
                });

              });

            });
          });
      };
    });
  }
});




/* UPDATE employees */
router.put('/', passport.authenticate('jwt', {
  //  console.log('inside train put ', req.body);
 
  session: false
}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    jwt.verify(token, settings.secret, (err, decoded) => {
      console.log (req.body.old_emp_id);
      if (err) {
        return res.json({
          success: false,
          message: 'Token is not valid'
        });
      } else {


        if (!req.body.emp_id) {
          return res.json({
            success: false,
            msg: 'Please pass emp id'
          });
        }
      }


      var onrcase = {
        emp_id:req.body.emp_id,
        Name: req.body.Name,
        'pf no': req.body['pf no'],
        'Present Designation': req.body['Present Designation'],
        'Posting Shed': req.body['Posting Shed'],
        Qualifications: req.body.Qualifications,
        'Date of Appointment': req.body['Date of Appointment'],
        'Date of Birth': req.body['Date of Birth'],
        CUG_NO: req.body.CUG_NO,
        Status: req.body.Status,
      }

      pool.query('Update employees SET ? WHERE emp_id = ?', [onrcase, req.body.old_emp_id],
        function(err, results, fields) {
          if (err) {
            return res.status(401).send({
              success: false,
              msg: err
            });
          }
          // var query = pool.query('INSERT INTO employees SET ?', onrcase, function(err, results, fields) {
          //   if (err) {
          //     return res.status(401).send({
          //       success: false,
          //       msg: err
          //     });
          //   }
          var query = pool.query(`INSERT INTO logbook (username,user_type,emp_id,event_type)
         values  ('${decoded.username}','${decoded.user_type}',${req.body.emp_id},'Edit')`, function(err, results2, fields) {
            if (err) {
              return res.status(401).send({
                success: false,
                msg: err
              });
            }

            return res.json({
              success: true,
              msg: 'Successfully updated emp id and log',
              results: results2
            });
          });
        })
    });
  }
});

router.put('/address', passport.authenticate('jwt', {
  //  console.log('inside train put ', req.body);
  session: false
}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    jwt.verify(token, settings.secret, (err, decoded) => {
      if (err) {
        return res.json({
          success: false,
          message: 'Token is not valid'
        });
      } else {


        if (!req.body.emp_id) {
          return res.json({
            success: false,
            msg: 'Please pass emp id'
          });
        }
      }


      var address = {
        'Present Address': req.body['Present Address'],
        'Present City': req.body['Present City'],
        'Present District': req.body['Present District'],
        'Present P O': req.body['Present P O'],
        'Present PIN': req.body['Present PIN'],
        'Present Phone No': req.body['Present Phone No'],
        'Present State': req.body['Present State'],
        'Permanent Address': req.body['Permanent Address'],
        'Permanent City': req.body['Permanent City'],
        'Permanent District': req.body['Permanent District'],
        'Permanent P O': req.body['Permanent P O'],
        'Permanent PIN': req.body['Permanent PIN'],
        'Permanent Phone No': req.body['Permanent Phone No'],
        'Permanent State': req.body['Permanent State'],
      }
      pool.query('Update employees SET ? WHERE emp_id = ?', [address, req.body.emp_id],
        function(err, results, fields) {
          if (err) {
            return res.status(401).send({
              success: false,
              msg: err
            });
          }
          // var query = pool.query('INSERT INTO employees SET ?', onrcase, function(err, results, fields) {
          //   if (err) {
          //     return res.status(401).send({
          //       success: false,
          //       msg: err
          //     });
          //   }
          var query = pool.query(`INSERT INTO logbook (username,user_type,emp_id,event_type)
         values  ('${decoded.username}','${decoded.user_type}',${req.body.emp_id},'Edit Address')`, function(err, results2, fields) {
            if (err) {
              return res.status(401).send({
                success: false,
                msg: err
              });
            }

            return res.json({
              success: true,
              msg: 'Successfully updated address and log',
              results: results2
            });
          });
        })
    });
  }
});

router.put('/promotion', passport.authenticate('jwt', {
  //  console.log('inside train put ', req.body);
  session: false
}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    jwt.verify(token, settings.secret, (err, decoded) => {
      if (err) {
        return res.json({
          success: false,
          message: 'Token is not valid'
        });
      } else {


        if (!req.body.emp_id) {
          return res.json({
            success: false,
            msg: 'Please pass emp id'
          });
        }
      }


      var promotion = {
        'Date of Promotion Rgular as ET': req.body['Date of Promotion Rgular as ET'],
        'Date of Promotion Rgular as Goods Driver': req.body['Date of Promotion Rgular as Goods Driver'],
        'Date of Promotion as Rgular  Pass Driver': req.body['Date of Promotion as Rgular  Pass Driver'],
        'Date of Promotion Regular as Spl Grade Driver': req.body['Date of Promotion Regular as Spl Grade Driver'],
      }

      pool.query('Update employees SET ? WHERE emp_id = ?', [promotion, req.body.emp_id],
        function(err, results, fields) {
          if (err) {
            return res.status(401).send({
              success: false,
              msg: err
            });
          }
          // var query = pool.query('INSERT INTO employees SET ?', onrcase, function(err, results, fields) {
          //   if (err) {
          //     return res.status(401).send({
          //       success: false,
          //       msg: err
          //     });
          //   }
          var query = pool.query(`INSERT INTO logbook (username,user_type,emp_id,event_type)
         values  ('${decoded.username}','${decoded.user_type}',${req.body.emp_id},'Edit promotion')`, function(err, results2, fields) {
            if (err) {
              return res.status(401).send({
                success: false,
                msg: err
              });
            }

            return res.json({
              success: true,
              msg: 'Successfully updated promotion and log',
              results: results2
            });
          });
        })
    });
  }
});
router.put('/pme', passport.authenticate('jwt', {
  //  console.log('inside train put ', req.body);
  session: false
}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    jwt.verify(token, settings.secret, (err, decoded) => {
      if (err) {
        return res.json({
          success: false,
          message: 'Token is not valid'
        });
      } else {
      if (!req.body.emp_id) {
          return res.json({
            success: false,
            msg: 'Please pass emp id'
          });
        }
      }


      var pme = {

        'Last PME done on': req.body['Last PME done on'],
        'PME due on': req.body['PME due on'],
        'Detail of PMEs': req.body['Detail of PMEs'],
        'With Spectacles': req.body['With Spectacles'],
        'With NV Spectacles': req.body['With NV Spectacles'],
      }

      pool.query('Update employees SET ? WHERE emp_id = ?', [pme, req.body.emp_id],
        function(err, results, fields) {
          if (err) {
            return res.status(401).send({
              success: false,
              msg: err
            });
          }
          // var query = pool.query('INSERT INTO employees SET ?', onrcase, function(err, results, fields) {
          //   if (err) {
          //     return res.status(401).send({
          //       success: false,
          //       msg: err
          //     });
          //   }
          var query = pool.query(`INSERT INTO logbook (username,user_type,emp_id,event_type)
         values  ('${decoded.username}','${decoded.user_type}',${req.body.emp_id},'Edit pme')`, function(err, results2, fields) {
            if (err) {
              return res.status(401).send({
                success: false,
                msg: err
              });
            }

            return res.json({
              success: true,
              msg: 'Successfully updated pme and log',
              results: results2
            });
          });
        })
    });
  }
});

router.put('/refresher', passport.authenticate('jwt', {
  //  console.log('inside train put ', req.body);
  session: false
}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    jwt.verify(token, settings.secret, (err, decoded) => {
      if (err) {
        return res.json({
          success: false,
          message: 'Token is not valid'
        });
      } else {


        if (!req.body.emp_id) {
          return res.json({
            success: false,
            msg: 'Please pass emp id'
          });
        }
      }


      var refresher = {
        'Last ZTC done on': req.body['Last ZTC done on'],
        'ZTC due on': req.body['ZTC due on'],
        'Detail of ZTC': req.body['Detail of ZTC'],
        'Last Technical Ref done on': req.body['Last Technical Ref done on'],
        'Technical Ref due on': req.body['Technical Ref due on'],
        'Detail ofTechnical Refresher': req.body['Detail ofTechnical Refresher'],
        'Last Safety Camp or seminar attended on': req.body['Last Safety Camp or seminar attended on'],
        'Detail of Safety Camp or seminar': req.body['Detail of Safety Camp or seminar'],
      }

      pool.query('Update employees SET ? WHERE emp_id = ?', [refresher, req.body.emp_id],
        function(err, results, fields) {
          if (err) {
            return res.status(401).send({
              success: false,
              msg: err
            });
          }
          // var query = pool.query('INSERT INTO employees SET ?', onrcase, function(err, results, fields) {
          //   if (err) {
          //     return res.status(401).send({
          //       success: false,
          //       msg: err
          //     });
          //   }
          var query = pool.query(`INSERT INTO logbook (username,user_type,emp_id,event_type)
         values  ('${decoded.username}','${decoded.user_type}',${req.body.emp_id},'Edit refresher')`, function(err, results2, fields) {
            if (err) {
              return res.status(401).send({
                success: false,
                msg: err
              });
            }

            return res.json({
              success: true,
              msg: 'Successfully updated refresher and log',
              results: results2
            });
          });
        })
    });
  }
});






/* DELETE BOOK */
router.delete('/', passport.authenticate('jwt', {
  //  console.log('inside train put ', req.body);
  session: false
}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    jwt.verify(token, settings.secret, (err, decoded) => {
      // if (err) {
      //   return res.json({
      //     success: false,
      //     message: 'Token is not valid'
      //   });
      // } else {
      //
      //
      //   if (!req.body.loco_no) {
      //     return res.json({
      //       success: false,
      //       msg: 'Please pass loco No'
      //     });
      //   }
      // }

      //  console.log('inside employees delete ', req.query);
      pool.query('delete from employees WHERE id = ?',
        [req.query.id],
        function(err, results, fields) {
          if (err) {
            return res.status(401).send({
              success: false,
              msg: err
            });
          }
          var query = pool.query(`INSERT INTO logbook (username,user_type,document_id,event_type)
         values  ('${decoded.username}','${decoded.user_type}','${req.query.id}','Delete')`, function(err, results3, fields) {
            if (err) {
              return res.status(401).send({
                success: false,
                msg: err
              });
            }



            return res.json({
              success: true,
              msg: 'Successfully deleted Train No.'

            });
          });
        });
    });
  }
});

router.put('/addtoken', function (req, res, next) {
  console.log(req.body.fcm_token)
  console.log(req.body.emp_id)
  if (req.body) {
    // console.log(req.body.rroom_id)
    // console.log(req.body.room_id)
    // var token = {
    //   token: req.body.fcm_token,
    //   emp_id: req.body.emp_id,
     
     
    // }
    pool.query('Update employees SET fcm_token = ? WHERE emp_id = ? ', [req.body.fcm_token,req.body.emp_id],
    function(err, results, fields) {
      console.log(err)
      if (err) {
        console.log(err)
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
   
       console.log(results);
      return res.json(results);
    
  })
  }
})

let now = moment();
// console.log(now.format());

const crypto = require('crypto');
var datetime = require('node-datetime');
const multer = require('multer');
// const db = require('db');
const fs = require('fs');
// const uuidv1 = require('uuid/v1');
const path = require('path');

const shell = require('shelljs');



var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    console.log(req.body)
    // initial upload path
    let destination = path.join('./public/files/'); // ./uploads/
    shell.mkdir('-p', './public/files/' + req.body.id);
    destination = path.join(destination, '', req.body.id); // ./uploads/files/generated-uuid-here/
    console.log('dest', destination)

    cb(
      null,
      destination
    );
  },
  filename: function(req, file, cb) {
    cb(null, moment().format('YYYY MM DD HH mm ss') + file.originalname)
  }

})

var upload = multer({
  storage: storage
})
// module.exports = (router) => {
//
//   router.get('/get', (req, res) => {
//     db.connect.query('SELECT * FROM employees',
//       {type: db.sequelize.QueryTypes.SELECT}
//     ).then(result => {
//       res.json(result);
//     })
//   });
//
//   router.post('/upload', upload.any(), (req, res) => {
//     res.json('test');
//
//
//   });
//
//   return router;
// };


router.post('/upload', upload.single('loco_trs'), (req, res, next) => {
  const file = req.file
  if (!file) {
    const error = new Error('Please upload a file')
    error.httpStatusCode = 400
    return next(error)
  }
  res.send(file)



})
router.post('/syncEmployees', function (req, res) {
     if (req.body.length > 0) {
      pool.getConnection(function (err, connection) {
        if (err) {
          return res.status(400).send(err)
        }
        util.bulkInsert(connection, 'employees', req.body, (err1, results) => {
          console.log(results)
          if (err1) {
            console.log(err1)
            return res.status(400).send(err1)
          } else {
            return res.json({
              success: true,
              msg: 'Successfully Synchronised Employees.'
            });
          }
        })
      })
    }
});

router.get('/get_changed_data', function(req, res) {
     pool.query('SELECT `employees`.`Name`,' +
     '`employees`.`Present Designation`,' +
     '`employees`.`Working as`,' +
     '`employees`.`Working at`,' +
     '`employees`.`Posting Shed`,' +
     '`employees`.`Gradation`,' +
     '`employees`.`Name of LI or SLI`,' +
     '`employees`.`Qualifications`,' +
     " DATE_FORMAT(`employees`.`Date of Appointment`, '%Y-%m-%d') `Date of Appointment`," +
     " DATE_FORMAT(`employees`.`Date of Birth`, '%Y-%m-%d') `Date of Birth`," +
     " DATE_FORMAT(`employees`.`Date of joining in AC Traction`, '%Y-%m-%d') `Date of joining in AC Traction`," +
     '`employees`.`pf no`,' +
     '`employees`.`emp_id`,' +
     " DATE_FORMAT(`employees`.`Date of joining as Asst Driver`, '%Y-%m-%d') as `Date of joining as Asst Driver`," +
     " DATE_FORMAT(`employees`.`Date of Promotion Rgular as ET`, '%Y-%m-%d') `Date of Promotion Rgular as ET`," +
     " DATE_FORMAT(`employees`.`Date of Promotion Rgular as Goods Driver`, '%Y-%m-%d') `Date of Promotion Rgular as Goods Driver`," +
     " DATE_FORMAT(`employees`.`Date of Promotion as Rgular  Pass Driver`, '%Y-%m-%d') `Date of Promotion as Rgular  Pass Driver`," +
     " DATE_FORMAT(`employees`.`Date of Promotion Regular as Spl Grade Driver`, '%Y-%m-%d') `Date of Promotion Regular as Spl Grade Driver`," +
     '`employees`.`Detail of Promotions`,' +
     " DATE_FORMAT(`employees`.`Date of Retirement`, '%Y-%m-%d') as `Date of Retirement`," +
     " DATE_FORMAT(`employees`.`Last PME done on`, '%Y-%m-%d') `Last PME done on`," +
     " DATE_FORMAT(`employees`.`PME due on`, '%Y-%m-%d') `PME due on`," +
     '`employees`.`Detail of PMEs`,' +
     '`employees`.`With Spectacles`,' +
     '`employees`.`With NV Spectacles`,' +
     " DATE_FORMAT(`employees`.`Last ZTC done on`, '%Y-%m-%d') `Last ZTC done on`," +
     " DATE_FORMAT(`employees`.`ZTC due on`, '%Y-%m-%d') `ZTC due on`," +
     "`employees`.`Detail of ZTC`," +
     " DATE_FORMAT(`employees`.`Last Technical Ref done on`, '%Y-%m-%d') `Last Technical Ref done on`," +
     " DATE_FORMAT(`employees`.`Technical Ref due on`, '%Y-%m-%d') `Technical Ref due on`," +
     '`employees`.`Detail ofTechnical Refresher`,' +
     " DATE_FORMAT(`employees`.`Last Safety Camp or seminar attended on`, '%Y-%m-%d') `Last Safety Camp or seminar attended on`," +
     '`employees`.`Detail of Safety Camp or seminar`,' +
     '`employees`.`Present Address`,' +
     '`employees`.`Present City`,' +
     '`employees`.`Present District`,' +
     '`employees`.`Present P O`,' +
     '`employees`.`Present PIN`,' +
     '`employees`.`Present Phone No`,' +
     '`employees`.`Present State`,' +
     '`employees`.`Permanent Address`,' +
     '`employees`.`Permanent City`,' +
     '`employees`.`Permanent District`,' +
     '`employees`.`Permanent P O`,' +
     '`employees`.`Permanent PIN`,' +
     '`employees`.`Permanent Phone No`,' +
     '`employees`.`Permanent State`,' +
     '`employees`.`Punishment`,' +
     '`employees`.`Rewards & acheivements`,' +
     '`employees`.`Status`,' +
     '`employees`.`Other informations`,' +
     '`employees`.`Q_TYPE`,' +
     '`employees`.`AGE_GROUP`,' +
     '`employees`.`WORKING IN`,' +
     '`employees`.`PASS_SRL`,' +
     '`employees`.`DESIG_SRL`,' +
     '`employees`.`FULL NAME`,' +
     '`employees`.`Uniform Received`,' +
     " DATE_FORMAT(`employees`.`Automatic Cert Due on`, '%Y-%m-%d') as `Automatic Cert Due on`," +
     '`employees`.`seniority_pos`,' +
     '`employees`.`CUG_NO`,' +
     '`employees`.`BILL_UNIT`,' +
     '`employees`.`CATEGORY`,' +
     '`employees`.`PSY_ATTND`,' +
     " DATE_FORMAT(`employees`.`PSY_DATE`, '%Y-%m-%d') as `PSY_DATE`," +
     '`employees`.`PSY_RESULT`,' +
     '`employees`.`PSY_DETAIL`,' +
      "SYS_CHANGE_OPERATION FROM `employees` where `Posting Shed` not in " + req.query.shed + " and updated_at >= ?", [req.query.updatedate], function(err, results, fields) {
      if (err) {
        console.log(err)
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      console.log(results)
      return res.json(results);
    });
});
// router.post('/upload', passport.authenticate('jwt', {
//   session: false
// }), function(req, res) {
//   var token = getToken(req.headers);
//   if (token) {
//     if (!req.files || Object.keys(req.files).length === 0) {
//       return res.status(400).send('No files were uploaded.');
//     }
//
//     // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
//     let sampleFile = req.files[0];
//
//     // Use the mv() method to place the file somewhere on your server
//     sampleFile.mv('/images/filename.jpg', function(err) {
//       if (err)
//         return res.status(500).send(err);
//
//       res.send('File uploaded!');
//     });
//     console.log(req.file)
//   }
//
// });





getToken = function(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = router;
