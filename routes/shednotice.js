var express = require('express');

 
var router = express.Router();
var passport = require('passport');
require('../config/passport')(passport);
var mysql = require('mysql');
var pool = require('../config/mysqlconnpool').pool;
var moment = require('moment');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var settings = require('../config/settings');
require('../config/passport')(passport);
var _ = require('lodash');
var util = require('../modules/util')
/* GET ALL TRAINS */
router.get('/shednoticeb_by_para', passport.authenticate('jwt', {
  session: false
}), function (req, res) {
  var token = getToken(req.headers);
  if (token) {
    pool.query(`select * from shednotice WHERE train_date= ? and shed = ? and link_type= ? and desig = ?`,
      [req.query.train_date, req.query.shed, req.query.link_type, req.query.desig],
      function (err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

router.get('/duties', function (req, res, next) {
  console.log(req.query)
  if (req.query.train_date) {
    req.query.train_date = moment(req.query.train_date, ["DD/MM/YYYY", "YYYY-MM-DD"]).tz('Asia/Kolkata').format('YYYY-MM-DD')
  }
  pool.query('SELECT DATE_FORMAT(train_date,"%d/%m/%Y") as train_date,shed,link_type,desig,link_no,day_no,work_no,work_seq,' +
    'duty_type,t_no_num,a.name,n_emp_id,org,DATE_FORMAT(dep,"%H:%i") as dep,dest,DATE_FORMAT(arr,"%H:%i") as arr, b.cug_no, c.cug_no as cli_cug from' +
    ' shednotice a left join employees b on a.n_emp_id = b.emp_id ' +
    ' left join supervisors c on b.`Name of LI or SLI` = c.emp_id' +
    '  where train_date= ? and shed= ? and link_type= ? and desig= ? order by n_emp_id,link_no, day_no, work_no,work_seq',
    [req.query.train_date, req.query.shed, req.query.link_type, req.query.desig], function (err, results, fields) {
      if (err) {
        console.log(err);
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      // console.log(results);
      var msges = _.groupBy(results, 'n_emp_id')
      var msg_array = []
      _.forEach(msges, e => {
        var cug_no = e[0]['cug_no']
        var cli_cug = e[0]['cli_cug']
        var e_name = e[0]['name']
        var train_date = e[0]['train_date']
        var msg = "Duty Date: " + train_date + " " + e_name
        _.forEach(e, d => {
          msg = msg + "\n" + d['work_seq'] + " " + d['duty_type'] + " " + d['t_no_num'] + " " +
            d['org'] + " " + d['dep'] + " " + d['dest'] + " " + d['arr']
        })
        msg_array.push(
          {
            train_date: train_date,
            name: e_name,
            cug_no: cug_no,
            cli_cug: cli_cug,
            msg: msg
          }
        )
      });
      // console.log(msg_array)
      return res.json(msg_array);
    })
    
});
router.get('/getDuties', function (req, res, next) {
  console.log(req.query)
  // if (req.query.train_date) {
  //   req.query.train_date = moment(req.query.train_date, ["DD/MM/YYYY", "YYYY-MM-DD"]).tz('Asia/Kolkata').format('YYYY-MM-DD')
  // }
  duty_date= moment(req.query.duty_date, 'DD/MM/YYYY').format('YYYY-MM-DD')
  end_date= moment(req.query.duty_date, 'DD/MM/YYYY').add(2, 'days').format('YYYY-MM-DD'),
  console.log(duty_date)
  pool.query(`SELECT DATE_FORMAT(train_date,'%d/%m/%Y') as train_date,shed,link_type,desig,link_no,day_no,work_no,work_seq,
  duty_type,t_no_num,a.name,n_emp_id,org,DATE_FORMAT(dep,'%H:%i') as dep,dest,DATE_FORMAT(arr,'%H:%i') as arr, b.cug_no from shednotice a left join employees b
     on a.n_emp_id = b.emp_id 
     where n_emp_id = ? and train_date >= ? and train_date < ? order by train_date,n_emp_id,link_no,day_no,work_no,work_seq`,
    [req.query.emp_id,duty_date, end_date], function (err, results, fields) {
      if (err) {
        console.log(err);
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      // console.log(results);
      var msges = _.groupBy(results, 'train_date')
      var msg_array = []
      _.forEach(msges, e => {
        var cug_no = e[0]['cug_no']
        var e_name = e[0]['name']
        var train_date = e[0]['train_date']
       var msg = "Duty Date: " + train_date + "\n"
        _.forEach(e, d => {
          msg = msg + "\t" + d['work_seq']+"." + " " + d['duty_type'] + " " + d['t_no_num'] + " " +
          d['org'] + " " + d['dep'] + " " + d['dest'] + " " + d['arr'] + "\n"
      })
      msg = msg + "\n"
        msg_array.push(
          {
            // train_date: train_date,
            // name: e_name,
            // cug_no: cug_no,
            msg: msg
          }
        )
      });
      console.log(msg_array)
      return res.json(msg_array);
    })
});

router.get('/getDutiesUnderCLI', function (req, res, next) {
  console.log(req.query)
  // if (req.query.train_date) {
  //   req.query.train_date = moment(req.query.train_date, ["DD/MM/YYYY", "YYYY-MM-DD"]).tz('Asia/Kolkata').format('YYYY-MM-DD')
  // }
  duty_date= moment(req.query.duty_date, 'DD/MM/YYYY').format('YYYY-MM-DD'),
  end_date= moment(req.query.duty_date, 'DD/MM/YYYY').add(2, 'days').format('YYYY-MM-DD'),
  console.log(duty_date)
  
  pool.query('SELECT DATE_FORMAT(train_date,"%d/%m/%Y") as train_date,shed,link_type,desig,link_no,day_no,work_no,work_seq,' +
  'duty_type,t_no_num,a.name,n_emp_id,org,DATE_FORMAT(dep,"%H:%i") as dep,dest,DATE_FORMAT(arr,"%H:%i") as arr, b.cug_no, c.cug_no as cli_cug from' +
  ' shednotice a left join employees b on a.n_emp_id = b.emp_id ' +
  ' left join supervisors c on b.`Name of LI or SLI` = c.emp_id' +
   '  where c.old_id = ? and train_date >= ? and train_date < ? order by a.name,train_date,link_no,day_no,work_no,work_seq',
    [req.query.emp_id,duty_date, end_date], function (err, results, fields) {
      if (err) {
        console.log(err);
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      // console.log(results);
      var msges = _.groupBy(results, 'name')
      var msg_array = []
      _.forEach(msges, e => {
        var cug_no = e[0]['cug_no']
        var e_name = e[0]['name']
        var train_date = e[0]['train_date']
        var msg = "Name: " + e_name + " (" + cug_no + ")\n"
        _.forEach(e, d => {
          msg = msg + "\t" + d['train_date'] + " " + d['work_seq']+"." + " " + d['duty_type'] + " " + d['t_no_num'] + " " +
            d['org'] + " " + d['dep'] + " " + d['dest'] + " " + d['arr'] + "\n"
        })
        msg = msg + "\n"
        msg_array.push(
          {
            // train_date: train_date,
            // name: e_name,
            // cug_no: cug_no,
            msg: msg
          }
        )
      });
      console.log(msg_array)
      return res.json(msg_array);
    })
});


router.get('/duties1', function (req, res, next) {
  
  var admin = require("firebase-admin");


  var serviceAccount = require("./../rms.json");

  if (!admin.apps.length) {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://rmsapp-82bd8-default-rtdb.firebaseio.com"
  });
  
}
  var topic = 'general';
  
  var message = {
    data: {
      title: 'Message from Lobby',
      body: 'Please Check Duty message'
    },
    topic: topic
  };
  
  // Send a message to devices subscribed to the provided topic.
  admin.messaging().send(message)
    .then((response) => {
      // Response is a message ID string.
      console.log('Successfully sent message:', response);
      return res.status(200).send({
        success: true,
        msg: 'Successfully sent notification'
      });
    })
    .catch((error) => {
      console.log('Error sending notification:', error);
      return res.status(401).send({
        success: false,
        msg: 'Error sending notification'
      });
      
  });
    
});

router.post('/msg', function (req, res, next) {
  
  var admin = require("firebase-admin");

//send messages code
  var serviceAccount = require("./../rms.json");

  if (!admin.apps.length) {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://rmsapp-82bd8-default-rtdb.firebaseio.com"
  });
  
}
  var topic = 'general';
  console.log("msg :"  + req.body.msg)
  var message = {
    data: {
      title: 'Message from Lobby',
      body: req.body.msg,
      // click_action: "ChatActivity"
    },
    topic: topic
  };
  
  // Send a message to devices subscribed to the provided topic.
  admin.messaging().send(message)
    .then((response) => {
      // Response is a message ID string.
      console.log('Successfully sent message:', response);
      return res.status(200).send({
        success: true,
        msg: 'Successfully sent notification'
      });
    })
    .catch((error) => {
      console.log('Error sending notification:', error);
      return res.status(401).send({
        success: false,
        msg: 'Error sending notification'
      });
      
  });
  
      
  if (req.body) {
    // console.log(req.body.rroom_id)
   
    var msg = {
      
      messagebody: req.body.msg,
     

    }

    pool.query('INSERT INTO message SET ?', msg, function (err, results, fields) {
      console.log(err)
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      //  console.log(results);
      // return res.json(results);
    });
  }
    
});


router.post('/msg2', function (req, res, next) {
  
  var admin = require("firebase-admin");
var token = req.body.token;
console.log("token :"  + req.body.token)
//send messages code
  var serviceAccount = require("./../rms.json");

  if (!admin.apps.length) {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://rmsapp-82bd8-default-rtdb.firebaseio.com"
  });
  
}
  var topic = 'general';
  console.log("msg :"  + req.body.msg)
  var message = {
   
   data: {
      title: 'Message from Lobby',
      body: req.body.msg
    },
    // topic: topic
    token: token
  };
  
  // Send a message to devices subscribed to the provided topic.
  admin.messaging().send(message)
    .then((response) => {
      // Response is a message ID string.
      console.log('Successfully sent message:', response);
      return res.status(200).send({
        success: true,
        msg: 'Successfully sent notification'
      });
    })
    .catch((error) => {
      console.log('Error sending notification:', error);
      return res.status(401).send({
        success: false,
        msg: 'Error sending notification'
      });
      
  });
    
  if (req.body) {
    // console.log(req.body.rroom_id)
    console.log(req.body.emp_id)
    var msg = {
      
      messagebody: req.body.msg,
      emp_id: req.body.emp_id,

    }

    pool.query('INSERT INTO message SET ?', msg, function (err, results, fields) {
      console.log(err)
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      //  console.log(results);
      // return res.json(results);
    });
  }
});

  router.get('/getmsg', function (req, res, next) {
    console.log(req.query.emp_id)
    pool.query(`SELECT DATE_FORMAT(created_at, '%d-%m-%Y %H:%i:%s') as created_at , messagebody from message where emp_id = ? OR emp_id IS NULL ORDER BY  id DESC
    LIMIT 4`,
      [req.query.emp_id], function (err1, results1, fields) {
        console.log(req.query.emp_id)
        console.log(err1)
        if (err1) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        console.log(results1);
        return res.json(results1);
      });
  });



  router.get('/getmsg2', function (req, res, next) {
    console.log(req.query.emp_id)
    pool.query(`SELECT date_format(created_at, '%d-%m-%Y') as date,
    date_format(created_at, '%H:%i:%s') as time, messagebody from message where emp_id = ? OR emp_id IS NULL ORDER BY  id DESC
    LIMIT 1 `,
      [req.query.emp_id], function (err1, results1, fields) {
        console.log(req.query.emp_id)
        console.log(err1)
        if (err1) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        console.log(results1);
        var msges = _.groupBy(results1,'date')
        var msg_array = []
        _.forEach(msges, e => {
          var date = e[0]['date']
          var time = e[0]['time']
          var messagebody = e[0]['messagebody']
         
          var msg = messagebody 
          _.forEach(e=> {
            msg = msg
          })
          msg =date +"\xa0" +"                                                                         "+time+ "\n" + "\n"+ msg + "\n"+ "\n"
          msg_array.push(
            {
              // train_date: train_date,
              // name: e_name,
              // cug_no: cug_no,
              msg: msg
            }
          )
        });
        console.log(msg_array)
        return res.json(msg_array);
      })
  });
  














const fs = require("fs");
const fastcsv = require('@fast-csv/parse');
var multer = require('multer');
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './files/uploads')
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname + '.csv')
  }
});
var upload = multer({
  storage: storage
}).single('file')
var util = require('./../modules/util')


router.post('/shednotice_upload', function (req, res) {
  upload(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      res.status(401).send({
        success: false,
        msg: err
      });
    } else if (err) {
      res.status(401).send({
        success: false,
        msg: err
      });
    } else if (req.file) {
      let stream = fs.createReadStream("./files/uploads/file.csv");
      let csvData = [];
      let csvStream = fastcsv
        .parse()
        .on("data", function (data) {
          csvData.push(data);
        })
        .on("end", function () {
          // remove the first line: header
          csvData.shift();
          if (csvData.length > 0) {
            for (i = 0; i < csvData.length; i++) {
              csvData[i][0] = moment(csvData[i][0], ['DD/MM/YYYY', 'YYYY-MM-DD']).format('YYYY-MM-DD')
              if (csvData[i][1] == '') {
                csvData[i][1] = 0
              }
              // console.log(csvData[i][13], csvData[i][15])
              if (csvData[i][13] && csvData[i][13].length == 5) {
                csvData[i][13] = moment(csvData[i][13], ['DD/MM/YYYY HH:mm:ss', 'YYYY-MM-DD HH:mm:ss']).format('YYYY-MM-DD HH:mm:ss')
              }
              if (csvData[i][15] && csvData[i][15].length == 5) {
                csvData[i][15] = moment(csvData[i][15], ['DD/MM/YYYY HH:mm:ss', 'YYYY-MM-DD HH:mm:ss']).format('YYYY-MM-DD HH:mm:ss')
              }
            }
            pool.getConnection(function (err, connection) {
              if (err) {
                console.error(err);
              }
              var query = mysql.format('DELETE FROM shednotice WHERE train_date= ? and shed = ? and link_type= ? and desig = ?',
                [moment(csvData[0][0]).format('YYYY-MM-DD'), csvData[0][1],
                csvData[0][2], csvData[0][3]])
              console.log(query)
              pool.query(query, (error, response) => {
                console.log(error, response);
                let query1 = `INSERT ignore INTO shednotice (train_date,shed,link_type,desig,link_no,day_no,work_no,work_seq,
                duty_type,t_no_num,name,n_emp_id,org,dep,dest,arr) VALUES ?`;
                connection.query(query1, [csvData], (error1, response1) => {
                  console.log(error1, response1);
                });
              });
            });
          }
          // open the connection

        });
      stream.pipe(csvStream);
      res.json({
        success: true,
        msg: "Shednotice uploaded successfully."
      });
    } else {
      res.status(401).send({
        success: false,
        msg: "Error in uploading, try again."
      });
    }
  })
})

router.post('/employees_upload', function (req, res) {
  upload(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      console.log(err)
      res.status(401).send({
        success: false,
        msg: err
      });
    } else if (err) {
      console.log(err)
      res.status(401).send({
        success: false,
        msg: err
      });
    } else if (req.file) {
      let dateColumns = [
      'Date of Appointment',
      'Date of Birth',
      'Date of joining in AC Traction',
      'Date of joining as Asst Driver',
      'Date of Promotion as Sr Asst Driver',
      'Date of Promotion AD HOC as ET',
      'Date of Promotion Rgular as ET',
      'Date of Promotion as Sr ET',
      'Date of Promotion AD HOC as Goods Driver',
      'Date of Promotion Rgular as Goods Driver',
      'Date of Promotion as Sr Goods Driver',
      'Date of Promotion AD HOC as Pass Driver',
      'Date of Promotion as Rgular  Pass Driver',
      'Date of Promotion as Sr Pass Driver',
      'Date of Promotion AD HOC as Spl Grade Driver',
      'Date of Promotion Regular as Spl Grade Driver',
      'Date of attaining 45 years',
      'Date of attaining 55 years',
      'Date of Retirement',
      'Last PME done on',
      'PME due on',
      'Last ZTC done on',
      'ZTC due on',
      'Last Technical Ref done on',
      'Technical Ref due on',
      'Last Safety Camp or seminar attended on',
      'Automatic Cert Due on',
      'Last_F_Plate',
      'Last_Counsell',
      'ABB_trg_From_P',
      'ABB_trg_to_P',
      'ABB_trg_From',
      'ABB_trg_to',
      'ABB_trg_due',
      'Simulator_trg_From',
      'Simulator_trg_to',
      'Simulator_trgdue',
      'StaticConv_trg_From',
      'StaticConv_trg_to',
      'StaticConv_trgdue',
      'CBC_trg_From',
      'CBC_trg_to',
      'ZTC_FROM',
      'ZTC_FROM_PREV',
      'ZTC_TO_PREV',
      'ELTC_FROM',
      'ELTC_FROM_PREV',
      'ELTC_TO_PREV',
      'SAFETYCAMP_FROM',
      'INHOUSE_FROM',
      'InHouseTrgDone',
      'InHouseTrgDue',
      'SafetyCampDue',
      'grade_dt',
      'Simulator_ABB_trg_From',
      'Simulator_ABB_trg_to',
      'Simulator_ABB_trgdue',
      'cms_id',
      'loco_emu']
      let keys = []
      let arrayOfobj = []
      let stream = fs.createReadStream("./files/uploads/file.csv");
      let csvData = [];
      let csvStream = fastcsv
        .parse()
        .on("data", function (data) {
          csvData.push(data);
          keys = csvData[0]
        })
        .on("end", function () {
          // remove the first line: header
          csvData.shift();
          if (csvData.length > 0) {
            for (i = 0; i < csvData.length; i++) {
              // console.log(csvData[i][13], csvData[i][15])
              // for (k=0;k< csvData[i].length; k++) {
              //   if (csvData[i][k]) {
              //     if (dateColumns.indexOf(keys[k]) > -1) {
              //       csvData[i][k] = moment(csvData[i][k].substr(1, 10), ['DD/MM/YYYY', 'YYYY-MM-DD']).format('YYYY-MM-DD')
              //     }
              //   }
              // }
              var obj = {}
              for(j=0; j < keys.length; j++) {
                obj[keys[j]] = csvData[i][j]
              }
               arrayOfobj.push(obj)
            }
            // console.log(arrayOfobj)
            pool.getConnection(function (err, connection) {
              if (err) {
                console.error(err);
                res.status(401).send({
                  success: false,
                  msg: "Error in uploading, try again."
                });
              }
              util.bulkInsert(connection, "employees",arrayOfobj,(err1, results)=> {
                if (err1) {
                  console.log(err1)
                  res.status(401).send({
                    success: false,
                    msg: "Error in uploading, try again."
                  });
                } else {
                  res.status(200).send({
                    success: true,
                    msg: "Succesfully uploaded employees"
                  });
                }
              })
            })
           }
        });
      stream.pipe(csvStream);
    }
  })
})
router.get('/gettoken', function (req, res, next) {
  console.log(req.query.emp_id)
  
  if (req.query) {
    // console.log(req.body.rroom_id)
    // console.log(req.body.room_id)
    // var token = {
    //   token: req.body.fcm_token,
    //   emp_id: req.body.emp_id,
     
     
    // }
    pool.query('SELECT fcm_token FROM  employees  WHERE emp_id = ? ', [req.query.emp_id],
    function(err, results, fields) {
      
      if (err) {
        console.log(err)
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
   
       console.log(results);
      return res.json(results);
    
  })
  }
})


router.get('/getfiles', function(req, res){
  let arrayOfobj1 = []
    async = require('async');
    var dirPath = 'public/files/';
    fs.readdir(dirPath, function (err, filesPath) {
      if (err) throw err;
      filesPath = filesPath.map(function(filePath){ //generating paths to file
          return filePath;
         
      });
     
      // res.json({dirPath:filesPath});
      if (filesPath !== undefined)
      {
      for(i=1 ; i<= filesPath.length ; i++)
      {
      
      const obj ={ name : filesPath[i-1]};
      console.log( filesPath[i-1]);
      arrayOfobj1.push(obj);
        }
      }
      // const myArr = JSON.parse(JSON.stringify(arr));
      res.json( arrayOfobj1);
      console.log( arrayOfobj1);
      // async.map(filesPath, function(filePath, cb){ //reading files or dir
      //     fs.readFile(filePath, 'utf8', cb);
      // }, function(err, results) {
      //     // console.log(results); //this is state when all files are completely read
      //     res.send(results); //sending all data to client
      //     console.log(results)
      // });
  });
// res.send(results);  
// console.log('complete'); 
});

// const uuidv1 = require('uuid/v1');
const path = require('path');

const shell = require('shelljs');



router.get('/getrmsfiles', function(req, res){
  let arrayOfobj1 = []
    async = require('async');
    var dirPath = 'public/rms/';
    fs.readdir(dirPath, function (err, filesPath) {
      if (err) throw err;
      filesPath = filesPath.map(function(filePath){ //generating paths to file
          return filePath;
         
      });
     
      // res.json({dirPath:filesPath});
      if (filesPath !== undefined)
      {
      for(i=1 ; i<= filesPath.length ; i++)
      {
      
      const obj ={ name : filesPath[i-1]};
      console.log( filesPath[i-1]);
      arrayOfobj1.push(obj);
        }
      }
      // const myArr = JSON.parse(JSON.stringify(arr));
      res.json( arrayOfobj1);
      console.log( arrayOfobj1);
      // async.map(filesPath, function(filePath, cb){ //reading files or dir
      //     fs.readFile(filePath, 'utf8', cb);
      // }, function(err, results) {
      //     // console.log(results); //this is state when all files are completely read
      //     res.send(results); //sending all data to client
      //     console.log(results)
      // });
  });
// res.send(results);  
// console.log('complete'); 
});

// const uuidv1 = require('uuid/v1');
// const path = require('path');

// const shell = require('shelljs');
















var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    console.log(req.body)
    // initial upload path
    let destination = path.join('public/rms/'); // ./uploads/
    // shell.mkdir('-p', 'public/rms/');
    destination = path.join(destination); // ./uploads/files/generated-uuid-here/
    console.log('dest', destination)

    cb(
      null,
      destination
    );
  },
  filename: function(req, file, cb) {
    cb(null, file.originalname )
  }

})

var upload = multer({
  storage: storage
})
// module.exports = (router) => {

  // router.get('/get', (req, res) => {
  //   db.connect.query('SELECT * FROM inspection',
  //     {type: db.sequelize.QueryTypes.SELECT}
  //   ).then(result => {
  //     res.json(result);
  //   })
  // });

  // router.post('/upload', upload.any(), (req, res) => {
  //   res.json('test');


  // });

//   return router;
// };


router.post('/uploads', upload.single(''), (req, res, next) => {
  const file = req.file
  if (!file) {
    const error = new Error('Please upload a file')
    error.httpStatusCode = 400
    return next(error)
  }
  console.log(file);
  res.send(file)



})


      
      

getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = router;
