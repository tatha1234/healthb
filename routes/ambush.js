var express = require('express');
var router = express.Router();
var passport = require('passport');
require('../config/passport')(passport);
var pool = require('../config/mysqlconnpool').pool;
var moment = require('moment');
var _ = require('lodash');
var util = require('./../modules/util');

/* GET ALL BOOKS */
router.get('/', function(req, res) {
  // var token = getToken(req.headers);
  // if (token) {
  pool.query(`SELECT * from clifb_ambush_check`, function(err, results, fields) {
    if (err) {
      return res.status(401).send({
        success: false,
        msg: err
      });
    }
    return res.json(results);
  });
  // } else {
  //   return res.status(403).send({
  //     success: false,
  //     msg: 'Unauthorized.'
  //   });
  // }
});

/* GET SINGLE BOOK BY ID */
router.get('/', function(req, res, next) {
  pool.query(`SELECT * from clifb_ambush_check where id = ?`, [req.query.id], function(err, results, fields) {
    if (err) {
      return res.status(401).send({
        success: false,
        msg: err
      });
    }
    console.log(results);
    return res.json(results);
  });
});

/* GET SINGLE BOOK BY ID */
router.post('/', function(req, res, next) {
  // console.log(req)
  if (req.body) {
    var ambush = {
      insp_list_id: req.body.insp_list_id,
      crew_id: req.body.crew_id,
      abnormality_id: req.body.abnormality_id,
      location_id: req.body.location_id,
      type: req.body.type,
      train_no: req.body.train_no,
      rake_no: req.body.rake_no,
      crew_name: req.body.crew_name,
      desig: req.body.desig,
      hq: req.body.hq,
      observation: req.body.observation
     
      
    }
    pool.query('INSERT INTO clifb_ambush_check SET ?', ambush, function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      //  console.log(results);
      return res.json(results);
    });
  }
})

router.put('/', function(req, res, next) {
  // console.log(req)
  if (req.body) {
    var ambush = {
      id : req.body.id,
      insp_list_id: req.body.insp_list_id,
      crew_id: req.body.crew_id,
      abnormality_id: req.body.abnormality_id,
      location_id: req.body.location_id,
      type: req.body.type,
      train_no: req.body.train_no,
      rake_no: req.body.rake_no,
      crew_name: req.body.crew_name,
      desig: req.body.desig,
      hq: req.body.hq,
      observation: req.body.observation
     
      
    }
    pool.query('UPDATE clifb_ambush_check SET ? WHERE id = ?', [ambush , ambush.id], function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      //  console.log(results);
      return res.json(results);
    });
  }
})



// router.put('/', function(req, res, next) {
//   //console.log(req)
//   if (req.body) {
//       var ambush = {
//         insp_list_id: req.body.insp_list_id,
//         crew_id: req.body.crew_id,
//         abnormality_id: req.body.abnormality_id,
//         location_id: req.body.location_id,
//         type: req.body.type,
//         train_no: req.body.train_no,
//         rake_no: req.body.rake_no,
//         crew_name: req.body.crew_name,
//         desig: req.body.desig,
//         hq: req.body.hq,
//         observation: req.body.observation
//     }
//     pool.query('UPDATE clifb_ambush_check SET ? where id = ?', [ambush, req.body.id], function(err, results, fields) {
//       if (err) {
//         return res.status(401).send({
//           success: false,
//           msg: err
//         });
//       }
//       if (req.body.ambush.length > 0) {
//         pool.getConnection(function(err, connection) {
//           if (err) {
//             return done(null);
//           }
//           util.bulkInsert(connection, 'clifb_ambush_check', req.body.ambush, (err1, results1) => {
//             if (err1) {
//               return res.status(401).send({
//                 success: false,
//                 msg: err1
//               });
//             }
//             //      console.log(final_ctr)
//             return res.json(results1);
//           });
//         });
//       } else {
//         return res.json(results);
//       }
//     });
//   }
// })

router.delete('/', function(req, res, next) {
  console.log(req.query)
  if (req.query) {
    pool.query('Delete from clifb_ambush_check where id = ?', [req.query.id], function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
        //  console.log(results);
        return res.json(results);

    });
  }
})
getToken = function(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = router;
