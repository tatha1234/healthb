var express = require('express');
var router = express.Router();
var passport = require('passport');
require('../config/passport')(passport);
var pool = require('../config/mysqlconnpool').pool;
var moment = require('moment');
var _ = require('lodash');
var util = require('../modules/util');

/* GET ALL BOOKS */
router.get('/', function (req, res) {
  // var token = getToken(req.headers);
  // if (token) {
  pool.query(`SELECT * from clifb_inspection_list`, function (err, results, fields) {
    if (err) {
      return res.status(401).send({
        success: false,
        msg: err
      });
    }
    return res.json(results);
  });
  // } else {
  //   return res.status(403).send({
  //     success: false,
  //     msg: 'Unauthorized.'
  //   });
  // }
});

/* GET SINGLE BOOK BY ID */
router.get('/id', function (req, res, next) {
  pool.query(`SELECT * from clifb_inspection_list where id = ?`, [req.query.id], function (err, results, fields) {
    if (err) {
      return res.status(401).send({
        success: false,
        msg: err
      });
    }
    console.log(results);
    return res.json(results);
  });
});

/* GET SINGLE BOOK BY ID */
router.post('/', function (req, res, next) {
  // console.log(req)
  if (req.body) {
    var fp_inspetion = {
      insp_list_id: req.body.insp_list_id,
      abnormality_id: req.body.abnormality_id,
      location_id: req.body.location_id,
      cli_id: req.body.cli_id,
      fb_uid: req.body.fb_uid,
      inspection_date: req.body.inspection_date,
      inspection_place: req.body.inspection_place,
      inspection_type: req.body.inspection_type,
      act_arr: req.body.act_arr,
      act_dep: req.body.act_dep,
      alp_id: req.body.alp_id,
      alp_name: req.body.alp_name,
      alp_hq: req.body.alp_hq,
      alp_desig: req.body.alp_desig,
      alp_observ: req.body.alp_observ,
      alp_pme: req.body.alp_pme,
      alp_tech: req.body.alp_tech,
      alp_tran: req.body.alp_tran,
      arr_remark: req.body.arr_remark,
      dep_remark: req.body.dep_remark,
      description: req.body.description,
      dest: req.body.dest,
      eng_abnormality: req.body.eng_abnormality,
      gd_hq: req.body.gd_hq,
      gd_name: req.body.gd_name,
      day_or_night: req.body.day_or_night,
      loco_base: req.body.loco_base,
      loco_last_insp_date: req.body.loco_last_insp_date,
      loco_last_insp_type: req.body.loco_last_insp_type,
      loco_no: req.body.loco_no,
      loco_observ: req.body.loco_observ,
      loco_poh: req.body.loco_poh,
      loco_type: req.body.loco_type,
      locations: req.body.locations,
      lp_abnormality: req.body.lp_abnormality,
      lp_cli: req.body.lp_cli,
      lp_cli_last_fp: req.body.lp_cli_last_fp,
      lp_desig: req.body.lp_desig,
      lp_grade: req.body.lp_grade,
      lp_hq: req.body.lp_hq,
      lp_id: req.body.lp_id,
      lp_name: req.body.lp_name,
      lp_observ: req.body.lp_observ,
      lp_pme: req.body.lp_pme,
      lp_tech: req.body.lp_tech,
      lp_tech_counseling: req.body.lp_tech_counseling,
      lp_tran: req.body.lp_tran,
      lp_tranp_counseling: req.body.lp_tranp_counseling,
      org: req.body.org,
      sch_arr: req.body.sch_arr,
      sch_dep: req.body.sch_dep,
      st_abnormality: req.body.st_abnormality,
      traffic_abnormality: req.body.traffic_abnormality,
      train_no: req.body.train_no,
      trd_abnormality: req.body.trd_abnormality,
      cli_name: req.body.cli_name,
      section: req.body.section,
      dateCreated: req.body.dateCreated,
      dateLastChanged: req.body.dateLastChanged
    }
    pool.query('INSERT INTO clifb_inspection_list SET ?', fp_inspetion, function (err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      //  console.log(results);
      return res.json(results);
    });
  }
})


router.put('/', function (req, res, next) {
  // console.log(req)
  if (req.body) {
    var fp_inspetion = {
      id : req.body.id,
      insp_list_id: req.body.insp_list_id,
      abnormality_id: req.body.abnormality_id,
      location_id: req.body.location_id,
      cli_id: req.body.cli_id,
      fb_uid: req.body.fb_uid,
      inspection_date: req.body.inspection_date,
      inspection_place: req.body.inspection_place,
      inspection_type: req.body.inspection_type,
      act_arr: req.body.act_arr,
      act_dep: req.body.act_dep,
      alp_id: req.body.alp_id,
      alp_name: req.body.alp_name,
      alp_hq: req.body.alp_hq,
      alp_desig: req.body.alp_desig,
      alp_observ: req.body.alp_observ,
      alp_pme: req.body.alp_pme,
      alp_tech: req.body.alp_tech,
      alp_tran: req.body.alp_tran,
      arr_remark: req.body.arr_remark,
      dep_remark: req.body.dep_remark,
      description: req.body.description,
      dest: req.body.dest,
      eng_abnormality: req.body.eng_abnormality,
      gd_hq: req.body.gd_hq,
      gd_name: req.body.gd_name,
      day_or_night: req.body.day_or_night,
      loco_base: req.body.loco_base,
      loco_last_insp_date: req.body.loco_last_insp_date,
      loco_last_insp_type: req.body.loco_last_insp_type,
      loco_no: req.body.loco_no,
      loco_observ: req.body.loco_observ,
      loco_poh: req.body.loco_poh,
      loco_type: req.body.loco_type,
      locations: req.body.locations,
      lp_abnormality: req.body.lp_abnormality,
      lp_cli: req.body.lp_cli,
      lp_cli_last_fp: req.body.lp_cli_last_fp,
      lp_desig: req.body.lp_desig,
      lp_grade: req.body.lp_grade,
      lp_hq: req.body.lp_hq,
      lp_id: req.body.lp_id,
      lp_name: req.body.lp_name,
      lp_observ: req.body.lp_observ,
      lp_pme: req.body.lp_pme,
      lp_tech: req.body.lp_tech,
      lp_tech_counseling: req.body.lp_tech_counseling,
      lp_tran: req.body.lp_tran,
      lp_tranp_counseling: req.body.lp_tranp_counseling,
      org: req.body.org,
      sch_arr: req.body.sch_arr,
      sch_dep: req.body.sch_dep,
      st_abnormality: req.body.st_abnormality,
      traffic_abnormality: req.body.traffic_abnormality,
      train_no: req.body.train_no,
      trd_abnormality: req.body.trd_abnormality,
      cli_name: req.body.cli_name,
      section: req.body.section,
      dateCreated: req.body.dateCreated,
      dateLastChanged: req.body.dateLastChanged
    }
    pool.query('UPDATE clifb_inspection_list SET ? WHERE id = ?', [fp_inspetion , fp_inspetion.id], function (err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      //  console.log(results);
      return res.json(results);
    });
  }
})



// router.put('/', function (req, res, next) {
//   //console.log(req)
//   if (req.body) {
//     var fp_inspetion = {
//         insp_list_id: req.body.insp_list_id,
//         abnormality_id: req.body.abnormality_id,
//         location_id: req.body.location_id,
//         cli_id: req.body.cli_id,
//         fb_uid: req.body.fb_uid,
//         inspection_date: req.body.inspection_date,
//         inspection_place: req.body.inspection_place,
//         inspection_type: req.body.inspection_type,
//         act_arr: req.body.act_arr,
//         act_dep: req.body.act_dep,
//         alp_id: req.body.alp_id,
//         alp_name: req.body.alp_name,
//         alp_hq: req.body.alp_hq,
//         alp_desig: req.body.alp_desig,
//         alp_observ: req.body.alp_observ,
//         alp_pme: req.body.alp_pme,
//         alp_tech: req.body.alp_tech,
//         alp_tran: req.body.alp_tran,
//         arr_remark: req.body.arr_remark,
//         dep_remark: req.body.dep_remark,
//         description: req.body.description,
//         dest: req.body.dest,
//         eng_abnormality: req.body.eng_abnormality,
//         gd_hq: req.body.gd_hq,
//         gd_name: req.body.gd_name,
//         day_or_night: req.body.day_or_night,
//         loco_base: req.body.loco_base,
//         loco_last_insp_date: req.body.loco_last_insp_date,
//         loco_last_insp_type: req.body.loco_last_insp_type,
//         loco_no: req.body.loco_no,
//         loco_observ: req.body.loco_observ,
//         loco_poh: req.body.loco_poh,
//         loco_type: req.body.loco_type,
//         locations: req.body.locations,
//         lp_abnormality: req.body.lp_abnormality,
//         lp_cli: req.body.lp_cli,
//         lp_cli_last_fp: req.body.lp_cli_last_fp,
//         lp_desig: req.body.lp_desig,
//         lp_grade: req.body.lp_grade,
//         lp_hq: req.body.lp_hq,
//         lp_id: req.body.lp_id,
//         lp_name: req.body.lp_name,
//         lp_observ: req.body.lp_observ,
//         lp_pme: req.body.lp_pme,
//         lp_tech: req.body.lp_tech,
//         lp_tech_counseling: req.body.lp_tech_counseling,
//         lp_tran: req.body.lp_tran,
//         lp_tranp_counseling: req.body.lp_tranp_counseling,
//         org: req.body.org,
//         sch_arr: req.body.sch_arr,
//         sch_dep: req.body.sch_dep,
//         st_abnormality: req.body.st_abnormality,
//         traffic_abnormality: req.body.traffic_abnormality,
//         train_no: req.body.train_no,
//         trd_abnormality: req.body.trd_abnormality,
//         cli_name: req.body.cli_name,
//         section: req.body.section,
//         dateCreated: req.body.dateCreated,
//         dateLastChanged: req.body.dateLastChanged
//     }
//     if (req.body.fp_inspetion.length > 0) {
//       pool.getConnection(function (err, connection) {
//         if (err) {
//           return done(null);
//         }
//         util.bulkInsert(connection, 'clifb_inspection_list', req.body.fp_inspetion, (err, results) => {
//           if (err) {
//             return res.status(401).send({
//               success: false,
//               msg: err
//             });
//           }
//           return res.json(results);
//         });
//       });
//     } else {
//       return res.status(401).send({
//         success: false,
//         msg: "No inspetion provided"
//       });
//     }

//   }
// })

router.delete('/', function (req, res, next) {
  console.log(req.query)
  if (req.query) {
    pool.query('Delete from clifb_inspection_list where id = ?', [req.query.id], function (err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      //  console.log(results);
      return res.json(results);

    });
  }
})
getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = router;
