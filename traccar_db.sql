-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: traccar_db
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cautions`
--

DROP TABLE IF EXISTS `cautions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cautions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1179 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clifb_abnormality`
--

DROP TABLE IF EXISTS `clifb_abnormality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clifb_abnormality` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cli_id` int NOT NULL,
  `fb_uid` varchar(45) DEFAULT NULL,
  `inspection_id` int DEFAULT NULL,
  `report_date` date DEFAULT NULL,
  `report_place` varchar(45) DEFAULT NULL,
  `report_type` varchar(45) DEFAULT NULL,
  `report` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clifb_fp_inspection`
--

DROP TABLE IF EXISTS `clifb_fp_inspection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clifb_fp_inspection` (
  `id` int NOT NULL AUTO_INCREMENT,
  `insp_list_id` int DEFAULT NULL,
  `cli_id` int DEFAULT NULL,
  `fb_uid` varchar(45) DEFAULT NULL,
  `inspection_date` date DEFAULT NULL,
  `inspection_place` varchar(45) DEFAULT NULL,
  `inspection_type` varchar(45) DEFAULT NULL,
  `act_arr` time DEFAULT NULL,
  `act_dep` time DEFAULT NULL,
  `alp_id` int DEFAULT NULL,
  `alp_name` varchar(45) DEFAULT NULL,
  `alp_hq` varchar(45) DEFAULT NULL,
  `alp_desig` varchar(45) DEFAULT NULL,
  `alp_observ` varchar(45) DEFAULT NULL,
  `alp_pme` date DEFAULT NULL,
  `alp_tech` date DEFAULT NULL,
  `alp_tran` date DEFAULT NULL,
  `arr_remark` varchar(45) DEFAULT NULL,
  `dep_remark` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `dest` varchar(45) DEFAULT NULL,
  `abnormality_id` int DEFAULT NULL,
  `eng_abnormality` varchar(45) DEFAULT NULL,
  `gd_hq` varchar(45) DEFAULT NULL,
  `gd_name` varchar(45) DEFAULT NULL,
  `day_or_night` varchar(10) DEFAULT NULL,
  `loco_base` varchar(45) DEFAULT NULL,
  `loco_last_insp_date` date DEFAULT NULL,
  `loco_last_insp_type` varchar(45) DEFAULT NULL,
  `loco_no` varchar(45) DEFAULT NULL,
  `loco_observ` varchar(45) DEFAULT NULL,
  `loco_poh` varchar(45) DEFAULT NULL,
  `loco_type` varchar(45) DEFAULT NULL,
  `locations` varchar(45) DEFAULT NULL,
  `lp_abnormality` varchar(45) DEFAULT NULL,
  `lp_cli` varchar(45) DEFAULT NULL,
  `lp_cli_last_fp` varchar(45) DEFAULT NULL,
  `lp_desig` varchar(45) DEFAULT NULL,
  `lp_grade` varchar(45) DEFAULT NULL,
  `lp_hq` varchar(45) DEFAULT NULL,
  `lp_id` int DEFAULT NULL,
  `lp_name` varchar(45) DEFAULT NULL,
  `lp_observ` varchar(45) DEFAULT NULL,
  `lp_pme` date DEFAULT NULL,
  `lp_tech` date DEFAULT NULL,
  `lp_tech_counseling` varchar(45) DEFAULT NULL,
  `lp_tran` date DEFAULT NULL,
  `lp_tranp_counseling` varchar(45) DEFAULT NULL,
  `org` varchar(45) DEFAULT NULL,
  `sch_arr` time DEFAULT NULL,
  `sch_dep` time DEFAULT NULL,
  `st_abnormality` varchar(45) DEFAULT NULL,
  `traffic_abnormality` varchar(45) DEFAULT NULL,
  `train_no` int DEFAULT NULL,
  `trd_abnormality` varchar(45) DEFAULT NULL,
  `cli_name` varchar(45) DEFAULT NULL,
  `section` varchar(20) DEFAULT NULL,
  `dateCreated` datetime DEFAULT NULL,
  `dateLastChanged` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clifb_inspection_list`
--

DROP TABLE IF EXISTS `clifb_inspection_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clifb_inspection_list` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cli_id` int DEFAULT NULL,
  `fb_uid` varchar(45) DEFAULT NULL,
  `inspection_type` varchar(45) DEFAULT NULL,
  `inspection_date` date DEFAULT NULL,
  `inspection_place` varchar(45) DEFAULT NULL,
  `day_or_nght` varchar(5) DEFAULT NULL,
  `insert_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `cli` (`cli_id`) /*!80000 INVISIBLE */,
  KEY `insp_date` (`inspection_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clifb_post`
--

DROP TABLE IF EXISTS `clifb_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clifb_post` (
  `ALP` varchar(45) DEFAULT NULL,
  `uid` varchar(45) NOT NULL,
  `author` varchar(45) DEFAULT NULL,
  `body` varchar(45) DEFAULT NULL,
  `from` date DEFAULT NULL,
  `LPG` varchar(45) DEFAULT NULL,
  `LPM` varchar(45) DEFAULT NULL,
  `LPP` varchar(45) DEFAULT NULL,
  `LPS` varchar(45) DEFAULT NULL,
  `notice_date` date DEFAULT NULL,
  `reference` varchar(45) DEFAULT NULL,
  `starCount` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `to` date DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clifb_post_counselling`
--

DROP TABLE IF EXISTS `clifb_post_counselling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clifb_post_counselling` (
  `uid` varchar(45) NOT NULL,
  `ALP` varchar(45) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `counsel_date` date DEFAULT NULL,
  `counsel_place` varchar(45) DEFAULT NULL,
  `dateCreated` date DEFAULT NULL,
  `dateLastChanged` date DEFAULT NULL,
  `LPG` varchar(45) DEFAULT NULL,
  `LPM` varchar(45) DEFAULT NULL,
  `LPP` varchar(45) DEFAULT NULL,
  `LPS` varchar(45) DEFAULT NULL,
  `text` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ctr`
--

DROP TABLE IF EXISTS `ctr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ctr` (
  `pk` bigint NOT NULL AUTO_INCREMENT,
  `device_train_map_id` int DEFAULT NULL,
  `train_date` date NOT NULL,
  `train_no` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `srl_no` int NOT NULL,
  `stn_code` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `sch_arr` time DEFAULT NULL,
  `sch_dep` time DEFAULT NULL,
  `act_arr` time DEFAULT NULL,
  `act_dep` time DEFAULT NULL,
  `late_in_sec` int DEFAULT NULL,
  `late_in` time DEFAULT NULL,
  `late_out_sec` int DEFAULT NULL,
  `late_out` time DEFAULT NULL,
  `stop_time` time DEFAULT NULL,
  `geofence_enter_speed` double DEFAULT NULL,
  `start_pos` int DEFAULT NULL,
  `end_pos` int DEFAULT NULL,
  `tot_dist` double DEFAULT NULL,
  `pos_start_id` bigint DEFAULT NULL,
  `pos_dest_id` bigint DEFAULT NULL,
  `sch_run_time` int DEFAULT NULL,
  `act_run_time` int DEFAULT NULL,
  `loss_gain_on_run` int DEFAULT NULL,
  `loss_gain_on_run_formatted` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `avg_speed` double DEFAULT NULL,
  `sectional_dist` double DEFAULT NULL,
  `sec_max_speed` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `act_arr_formatted` datetime DEFAULT NULL,
  `act_dep_formatted` datetime DEFAULT NULL,
  PRIMARY KEY (`pk`),
  UNIQUE KEY `unique_index` (`train_date`,`train_no`,`srl_no`)
) ENGINE=InnoDB AUTO_INCREMENT=302611551 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cug_list`
--

DROP TABLE IF EXISTS `cug_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cug_list` (
  `id` int NOT NULL AUTO_INCREMENT,
  `sl_no` int NOT NULL,
  `cug_no` varchar(12) DEFAULT NULL,
  `holder_name` varchar(50) DEFAULT NULL,
  `designation` varchar(25) DEFAULT NULL,
  `pf_no` varchar(12) DEFAULT NULL,
  `dept` varchar(12) DEFAULT NULL,
  `plan` varchar(5) DEFAULT NULL,
  `remarks` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=932 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `databasechangelog`
--

DROP TABLE IF EXISTS `databasechangelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `databasechangelog` (
  `ID` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `databasechangeloglock`
--

DROP TABLE IF EXISTS `databasechangeloglock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `databasechangeloglock` (
  `ID` int NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `desig_srl`
--

DROP TABLE IF EXISTS `desig_srl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `desig_srl` (
  `SRL_NO` tinyint unsigned DEFAULT NULL,
  `SRL_NO1` tinyint unsigned DEFAULT NULL,
  `DESIG` varchar(50) NOT NULL,
  `C_DESIG` varchar(50) DEFAULT NULL,
  `C_DESIG_NEW` varchar(50) DEFAULT NULL,
  `DESIG_SRL` tinyint unsigned DEFAULT NULL,
  `NEW_DESIG` varchar(50) DEFAULT NULL,
  `NEW_DESIG_SHORT` varchar(50) DEFAULT NULL,
  `NEW_DESIG_SHORTEST` varchar(50) DEFAULT NULL,
  `C_NEW_DESIG_SHORTEST` varchar(45) DEFAULT NULL,
  `C_NEW_DESIG_SHORT` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`DESIG`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_pf`
--

DROP TABLE IF EXISTS `device_pf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `device_pf` (
  `id` int NOT NULL AUTO_INCREMENT,
  `deviceid` int NOT NULL,
  `beaconid` varchar(45) NOT NULL,
  `stn_code` varchar(5) NOT NULL,
  `pf` varchar(3) NOT NULL,
  `rssi` int DEFAULT NULL,
  `distance` double DEFAULT NULL,
  `devicetime` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_deviceid_time` (`deviceid`,`devicetime`) /*!80000 INVISIBLE */,
  KEY `idx_fixtime` (`devicetime`) /*!80000 INVISIBLE */
) ENGINE=InnoDB AUTO_INCREMENT=922 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_train_map`
--

DROP TABLE IF EXISTS `device_train_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `device_train_map` (
  `id` int NOT NULL AUTO_INCREMENT,
  `device_id` int NOT NULL,
  `device_name` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `unique_device_id` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `lastupdate` timestamp NULL DEFAULT NULL,
  `positionid` int DEFAULT NULL,
  `groupid` int DEFAULT NULL,
  `train_date` date NOT NULL,
  `train_no` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `train_no_text` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `org` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `dep` datetime DEFAULT NULL,
  `arr` datetime DEFAULT NULL,
  `dest` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `late_dep` int DEFAULT NULL,
  `late_arr` int DEFAULT NULL,
  `act_dep` datetime DEFAULT NULL,
  `act_arr` datetime DEFAULT NULL,
  `pf` char(5) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `next_train_no` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `next_train_no_id` int DEFAULT NULL,
  `next_train_description` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `last_stn_srl` int DEFAULT NULL,
  `last_stn_code` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `last_stn_arr` time DEFAULT NULL,
  `last_stn_dep` time DEFAULT NULL,
  `last_stn_late_arr` int DEFAULT NULL,
  `last_stn_late_dep` int DEFAULT NULL,
  `last_stn_pos_id` int DEFAULT NULL,
  `insert_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `time_entering_section` datetime DEFAULT NULL,
  `time_out_of_section` datetime DEFAULT NULL,
  `guess_pf` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `manual` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `manual_update_time` datetime DEFAULT NULL,
  `failure_report` varchar(500) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `act_arr_pf` datetime DEFAULT NULL,
  `auto_attach` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`device_id`,`train_date`,`train_no`),
  KEY `train_date` (`train_date`),
  KEY `arr` (`arr`,`dep`),
  KEY `dep_arr` (`dep`,`arr`,`act_arr`)
) ENGINE=InnoDB AUTO_INCREMENT=282025 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employees` (
  `Name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Present Designation` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Working as` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Working at` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Posting Shed` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Gradation` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Name of LI or SLI` int DEFAULT NULL,
  `Qualifications` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Date of Appointment` datetime(6) DEFAULT NULL,
  `Date of Birth` datetime(6) DEFAULT NULL,
  `Date of joining in AC Traction` datetime(6) DEFAULT NULL,
  `pf no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `emp_id` int NOT NULL AUTO_INCREMENT,
  `Date of joining as Asst Driver` datetime(6) DEFAULT NULL,
  `Date of Promotion as Sr Asst Driver` datetime(6) DEFAULT NULL,
  `Date of Promotion AD HOC as ET` datetime(6) DEFAULT NULL,
  `Date of Promotion Rgular as ET` datetime(6) DEFAULT NULL,
  `Date of Promotion as Sr ET` datetime(6) DEFAULT NULL,
  `Detail of Promotion as Sr ET` longtext,
  `Date of Promotion AD HOC as Goods Driver` datetime(6) DEFAULT NULL,
  `Date of Promotion Rgular as Goods Driver` datetime(6) DEFAULT NULL,
  `Date of Promotion as Sr Goods Driver` datetime(6) DEFAULT NULL,
  `Date of Promotion AD HOC as Pass Driver` datetime(6) DEFAULT NULL,
  `Date of Promotion as Rgular  Pass Driver` datetime(6) DEFAULT NULL,
  `Date of Promotion as Sr Pass Driver` datetime(6) DEFAULT NULL,
  `Date of Promotion AD HOC as Spl Grade Driver` datetime(6) DEFAULT NULL,
  `Date of Promotion Regular as Spl Grade Driver` datetime(6) DEFAULT NULL,
  `Detail of Promotions` longtext,
  `Date of attaining 45 years` datetime(6) DEFAULT NULL,
  `Date of attaining 55 years` datetime(6) DEFAULT NULL,
  `Date of Retirement` datetime(6) DEFAULT NULL,
  `Last PME done on` datetime(6) DEFAULT NULL,
  `PME due on` datetime(6) DEFAULT NULL,
  `Detail of PMEs` longtext,
  `With Spectacles` tinyint(1) DEFAULT NULL,
  `With NV Spectacles` tinyint(1) DEFAULT NULL,
  `Last ZTC done on` datetime(6) DEFAULT NULL,
  `ZTC due on` datetime(6) DEFAULT NULL,
  `Detail of ZTC` longtext,
  `Last Technical Ref done on` datetime(6) DEFAULT NULL,
  `Technical Ref due on` datetime(6) DEFAULT NULL,
  `Detail ofTechnical Refresher` longtext,
  `Last Safety Camp or seminar attended on` datetime(6) DEFAULT NULL,
  `Detail of Safety Camp or seminar` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Present Address` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Present City` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Present District` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Present P O` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Present PIN` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Present Phone No` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Present State` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Permanent Address` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Permanent City` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Permanent District` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Permanent P O` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Permanent PIN` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Permanent Phone No` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Permanent State` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Punishment` longtext,
  `Rewards & acheivements` longtext,
  `Status` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Other informations` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Q_TYPE` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `AGE_GROUP` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `WORKING IN` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `PASS_SRL` int DEFAULT NULL,
  `n_pf_no` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `old_pf_no` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `DESIG_SRL` tinyint unsigned DEFAULT NULL,
  `FULL NAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Uniform Received` tinyint(1) DEFAULT NULL,
  `Automatic Cert Due on` datetime(6) DEFAULT NULL,
  `seniority_pos` smallint DEFAULT NULL,
  `Page No` smallint DEFAULT NULL,
  `N_Designation` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `CUG_NO` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `BILL_UNIT` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `CATEGORY` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `PSY_ATTND` tinyint(1) DEFAULT NULL,
  `PSY_DATE` datetime(6) DEFAULT NULL,
  `PSY_RESULT` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `PSY_DETAIL` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `ts` varbinary(8) DEFAULT NULL,
  `Last_F_Plate` datetime(6) DEFAULT NULL,
  `LF_T_NO` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `LF_SEC` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Last_Counsell` datetime(6) DEFAULT NULL,
  `Councell_Subject` longtext,
  `Detail ofInHouseTrg` longtext,
  `ABB_trg_From_P` datetime(6) DEFAULT NULL,
  `ABB_trg_to_P` datetime(6) DEFAULT NULL,
  `ABB_result_P` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `ABB_trg_From` datetime(6) DEFAULT NULL,
  `ABB_trg_to` datetime(6) DEFAULT NULL,
  `ABB_trg_due` datetime(6) DEFAULT NULL,
  `ABB_result` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Simulator_trg_From` datetime(6) DEFAULT NULL,
  `Simulator_trg_to` datetime(6) DEFAULT NULL,
  `Simulator_trgdue` datetime(6) DEFAULT NULL,
  `Simulator_result` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `StaticConv_trg_From` datetime(6) DEFAULT NULL,
  `StaticConv_trg_to` datetime(6) DEFAULT NULL,
  `StaticConv_trgdue` datetime(6) DEFAULT NULL,
  `StaticConv_result` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `CBC_trg_From` datetime(6) DEFAULT NULL,
  `CBC_trg_to` datetime(6) DEFAULT NULL,
  `CBC_Place` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `CBC_result` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `CBC_remark` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `ZTC_FROM` datetime(6) DEFAULT NULL,
  `ZTC_result` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `ZTC_FROM_PREV` datetime(6) DEFAULT NULL,
  `ZTC_TO_PREV` datetime(6) DEFAULT NULL,
  `ZTC_result_PREV` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `ELTC_FROM` datetime(6) DEFAULT NULL,
  `ELTC_result` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `ELTC_FROM_PREV` datetime(6) DEFAULT NULL,
  `ELTC_TO_PREV` datetime(6) DEFAULT NULL,
  `ELTC_result_PREV` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `SAFETYCAMP_FROM` datetime(6) DEFAULT NULL,
  `SAFETYCAMP_result` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `INHOUSE_FROM` datetime(6) DEFAULT NULL,
  `InHouseTrgDone` datetime(6) DEFAULT NULL,
  `InHouseTrgDue` datetime(6) DEFAULT NULL,
  `INHOUSE_result` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `SafetyCampDue` datetime(6) DEFAULT NULL,
  `grade_dt` datetime(6) DEFAULT NULL,
  `prev_grade` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `drv_tech_a` int DEFAULT NULL,
  `drv_tech_b` int DEFAULT NULL,
  `drv_tech_c` int DEFAULT NULL,
  `drv_tech_d` int DEFAULT NULL,
  `drv_tech` int DEFAULT NULL,
  `knowl_safety_a` int DEFAULT NULL,
  `knowl_safety_b` int DEFAULT NULL,
  `knowl_safety_c` int DEFAULT NULL,
  `knowl_safety_d` int DEFAULT NULL,
  `knowl_safety_e` int DEFAULT NULL,
  `knowl_safety` int DEFAULT NULL,
  `tech_knowl_a` int DEFAULT NULL,
  `tech_knowl_b` int DEFAULT NULL,
  `tech_knowl_c` int DEFAULT NULL,
  `tech_knowl` int DEFAULT NULL,
  `personal_habits_a` int DEFAULT NULL,
  `personal_habits_b` int DEFAULT NULL,
  `personal_habits_c` int DEFAULT NULL,
  `personal_habits_d` int DEFAULT NULL,
  `personal_habits` int DEFAULT NULL,
  `accident_rec` int DEFAULT NULL,
  `total` int DEFAULT NULL,
  `Simulator_ABB_trg_From` datetime(6) DEFAULT NULL,
  `Simulator_ABB_trg_to` datetime(6) DEFAULT NULL,
  `Simulator_ABB_trgdue` datetime(6) DEFAULT NULL,
  `Simulator_ABB_result` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `father_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `mode_of_entry` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `shcedule_category` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `PSY_HS` tinyint(1) DEFAULT NULL,
  `gender` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `health_status` tinytext,
  `health_description` varchar(300) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `no_of_app_inst` int DEFAULT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10120 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `feedback summary`
--

DROP TABLE IF EXISTS `feedback summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feedback summary` (
  `ALP_FP` varchar(45) NOT NULL,
  `author` varchar(45) DEFAULT NULL,
  `Automatic_Ambush` varchar(45) DEFAULT NULL,
  `BA_Test` varchar(45) DEFAULT NULL,
  `Brake_Feel_Ambush` varchar(45) DEFAULT NULL,
  `Brake_Power_Ambush` varchar(45) DEFAULT NULL,
  `Cab_Checking` varchar(45) DEFAULT NULL,
  `Data_Analysis` varchar(45) DEFAULT NULL,
  `Flasher_Ambush` varchar(45) DEFAULT NULL,
  `Foot_Plate` varchar(45) DEFAULT NULL,
  `Gate_Ambush` varchar(45) DEFAULT NULL,
  `JoinSig_Sighting` varchar(45) DEFAULT NULL,
  `Lobby_Inspection` varchar(45) DEFAULT NULL,
  `LPG_A` varchar(45) DEFAULT NULL,
  `LPG_B` varchar(45) DEFAULT NULL,
  `LPG_C` varchar(45) DEFAULT NULL,
  `LPM_A` varchar(45) DEFAULT NULL,
  `LPM_B` varchar(45) DEFAULT NULL,
  `LPM_C` varchar(45) DEFAULT NULL,
  `LPP_A` varchar(45) DEFAULT NULL,
  `LPP_B` varchar(45) DEFAULT NULL,
  `LPP_C` varchar(45) DEFAULT NULL,
  `LPS_A` varchar(45) DEFAULT NULL,
  `LPS_B` varchar(45) DEFAULT NULL,
  `LPS_C` varchar(45) DEFAULT NULL,
  `Other_Inspection` varchar(45) DEFAULT NULL,
  `Platform_Duty` varchar(45) DEFAULT NULL,
  `Running_Room_Inspection` varchar(45) DEFAULT NULL,
  `Shed_Visit` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ALP_FP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `geofences`
--

DROP TABLE IF EXISTS `geofences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `geofences` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `description` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `area` varchar(4096) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=614 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gradation`
--

DROP TABLE IF EXISTS `gradation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gradation` (
  `dateCreated` date NOT NULL,
  `grade_date` date DEFAULT NULL,
  `text10` varchar(45) DEFAULT NULL,
  `text11` varchar(45) DEFAULT NULL,
  `text12` varchar(45) DEFAULT NULL,
  `text13` varchar(45) DEFAULT NULL,
  `text14` varchar(45) DEFAULT NULL,
  `text16` varchar(45) DEFAULT NULL,
  `text17` varchar(45) DEFAULT NULL,
  `text18` varchar(45) DEFAULT NULL,
  `text2` varchar(45) DEFAULT NULL,
  `text3` varchar(45) DEFAULT NULL,
  `text4` varchar(45) DEFAULT NULL,
  `text5` varchar(45) DEFAULT NULL,
  `text6` varchar(45) DEFAULT NULL,
  `text7` varchar(45) DEFAULT NULL,
  `text8` varchar(45) DEFAULT NULL,
  `text9` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`dateCreated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `health_status`
--

DROP TABLE IF EXISTS `health_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `health_status` (
  `id` int NOT NULL AUTO_INCREMENT,
  `status_date` date NOT NULL,
  `cli_id` int NOT NULL,
  `emp_id` int DEFAULT NULL,
  `health_status` text,
  `health_description` varchar(300) DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(45) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `no_of_app_inst` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`status_date`,`cli_id`,`emp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=68608 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `km_tbl`
--

DROP TABLE IF EXISTS `km_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `km_tbl` (
  `srl_no` int NOT NULL AUTO_INCREMENT,
  `st_stn` varchar(12) COLLATE latin1_general_ci DEFAULT NULL,
  `end_stn` varchar(12) COLLATE latin1_general_ci DEFAULT NULL,
  `km` decimal(12,2) DEFAULT NULL,
  `RUN_TIME` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`srl_no`)
) ENGINE=InnoDB AUTO_INCREMENT=16732 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `last_train`
--

DROP TABLE IF EXISTS `last_train`;
/*!50001 DROP VIEW IF EXISTS `last_train`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `last_train` AS SELECT 
 1 AS `device_id`,
 1 AS `max_arr`,
 1 AS `id`,
 1 AS `last_train_date`,
 1 AS `last_train_no`,
 1 AS `org`,
 1 AS `dep`,
 1 AS `dest`,
 1 AS `arr`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `logbook`
--

DROP TABLE IF EXISTS `logbook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `logbook` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `user_type` char(20) DEFAULT NULL,
  `document_id` int DEFAULT NULL,
  `date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `event_type` varchar(45) DEFAULT NULL,
  `emp_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73398 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `miss_trains`
--

DROP TABLE IF EXISTS `miss_trains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `miss_trains` (
  `train_no` int NOT NULL,
  `train_name` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `train_type` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `valid_from` date DEFAULT NULL,
  `valid_to` date DEFAULT NULL,
  `zone` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `divn` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `depot` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `days_of_run` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `dep` time DEFAULT NULL,
  `org` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `dest` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `arr` time DEFAULT NULL,
  `travel_time` time DEFAULT NULL,
  `total_kms` decimal(10,0) DEFAULT NULL,
  `avg_speed` decimal(10,0) DEFAULT NULL,
  `er` int DEFAULT NULL,
  `tr` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `miss_trains_tbl`
--

DROP TABLE IF EXISTS `miss_trains_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `miss_trains_tbl` (
  `pk` bigint unsigned NOT NULL DEFAULT '0',
  `train_no` int NOT NULL,
  `stn_name` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `stn_type` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `stn_code` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `division` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `zone` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `block_section` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `wtt_arr` time DEFAULT NULL,
  `wtt_day_arr` int DEFAULT NULL,
  `wtt_dep` time DEFAULT NULL,
  `wtt_day_dep` int DEFAULT NULL,
  `stop` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `ptt_arr` time DEFAULT NULL,
  `ptt_day_arr` int DEFAULT NULL,
  `ptt_dep` time DEFAULT NULL,
  `ptt_day_dep` int DEFAULT NULL,
  `pf` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `tr` int DEFAULT NULL,
  `er` int DEFAULT NULL,
  `distance` double DEFAULT NULL,
  `stn_sr_no` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `msg_table`
--

DROP TABLE IF EXISTS `msg_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `msg_table` (
  `msg_id` varchar(255) NOT NULL,
  `msg_date` date DEFAULT NULL,
  `msg_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `topic` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `msg_text` varchar(1000) DEFAULT NULL,
  UNIQUE KEY `unique_idx` (`msg_date`,`msg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pf`
--

DROP TABLE IF EXISTS `pf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pf` (
  `pk` bigint NOT NULL AUTO_INCREMENT,
  `train_date` date NOT NULL,
  `stn_code` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `train_no` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `device_id` int DEFAULT NULL,
  `device_name` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `pf` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`pk`),
  UNIQUE KEY `unique_index` (`train_date`,`train_no`,`stn_code`)
) ENGINE=InnoDB AUTO_INCREMENT=26866951 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pf_auto_msg`
--

DROP TABLE IF EXISTS `pf_auto_msg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pf_auto_msg` (
  `pk` bigint NOT NULL AUTO_INCREMENT,
  `insert_date` date DEFAULT NULL,
  `insert_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `stn_code` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `pf` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `msg_title` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `msg` varchar(500) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`pk`),
  KEY `idx` (`insert_date`,`insert_time`)
) ENGINE=InnoDB AUTO_INCREMENT=1458309 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pf_rake`
--

DROP TABLE IF EXISTS `pf_rake`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pf_rake` (
  `pk` bigint NOT NULL AUTO_INCREMENT,
  `stn_code` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `rake_no` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `device_id` int DEFAULT NULL,
  `pf` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `insert_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk`),
  KEY `idx` (`insert_time`,`stn_code`,`rake_no`)
) ENGINE=InnoDB AUTO_INCREMENT=606663 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pf_rake_enter`
--

DROP TABLE IF EXISTS `pf_rake_enter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pf_rake_enter` (
  `pk` bigint NOT NULL AUTO_INCREMENT,
  `stn_code` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `rake_no` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `device_id` int DEFAULT NULL,
  `pf` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `insert_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pf_date` date DEFAULT NULL,
  `pf_time` varchar(2) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`pk`),
  UNIQUE KEY `idx` (`pf_date`,`pf_time`,`stn_code`,`rake_no`)
) ENGINE=InnoDB AUTO_INCREMENT=3224262 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pf_train_rake`
--

DROP TABLE IF EXISTS `pf_train_rake`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pf_train_rake` (
  `pk` bigint NOT NULL AUTO_INCREMENT,
  `stn_code` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `train_no` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `rake_no` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `deviceid` int DEFAULT NULL,
  `device_name` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `pf` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `insert_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk`),
  UNIQUE KEY `PF_unique` (`stn_code`,`pf`)
) ENGINE=InnoDB AUTO_INCREMENT=5680821 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pincode_lat_lang`
--

DROP TABLE IF EXISTS `pincode_lat_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pincode_lat_lang` (
  `pincode_id` int NOT NULL AUTO_INCREMENT,
  `country` varchar(5) DEFAULT NULL,
  `pin_code` varchar(12) DEFAULT NULL,
  `post_office` varchar(255) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `district` varchar(25) DEFAULT NULL,
  `sub_division` varchar(45) DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  PRIMARY KEY (`pincode_id`),
  KEY `idx` (`pin_code`)
) ENGINE=InnoDB AUTO_INCREMENT=156567 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rake_dtl`
--

DROP TABLE IF EXISTS `rake_dtl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rake_dtl` (
  `id` int NOT NULL AUTO_INCREMENT,
  `deviceid` int NOT NULL,
  `rake_no` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `shed` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rake_no` (`rake_no`)
) ENGINE=InnoDB AUTO_INCREMENT=737 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rakelink`
--

DROP TABLE IF EXISTS `rakelink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rakelink` (
  `srl` int NOT NULL AUTO_INCREMENT,
  `rake_no` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `train_no` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `first_train_no` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `no_of_cars` int DEFAULT NULL,
  PRIMARY KEY (`srl`),
  UNIQUE KEY `train_no` (`train_no`)
) ENGINE=InnoDB AUTO_INCREMENT=1308 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rms_bed_bookings`
--

DROP TABLE IF EXISTS `rms_bed_bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rms_bed_bookings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `booking_date` date DEFAULT NULL,
  `emp_id` int DEFAULT NULL,
  `running_room_id` int DEFAULT NULL,
  `room_id` int DEFAULT NULL,
  `bed_id` int DEFAULT NULL,
  `duration` decimal(10,0) DEFAULT NULL,
  `check_in_time` datetime DEFAULT NULL,
  `check_out_time` datetime DEFAULT NULL,
  `cms_id` varchar(10) DEFAULT NULL,
  `train_no` varchar(6) DEFAULT NULL,
  `remarks` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `date_indx` (`booking_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rms_beds`
--

DROP TABLE IF EXISTS `rms_beds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rms_beds` (
  `id` int NOT NULL AUTO_INCREMENT,
  `running_room_id` int DEFAULT NULL,
  `room_id` int DEFAULT NULL,
  `bed_no` int DEFAULT NULL,
  `bed_desc` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rms_meal_bookings`
--

DROP TABLE IF EXISTS `rms_meal_bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rms_meal_bookings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `booking_date` date DEFAULT NULL,
  `emp_id` int DEFAULT NULL,
  `running_room_id` int DEFAULT NULL,
  `meal_type` varchar(10) DEFAULT NULL,
  `meal_status` varchar(20) DEFAULT NULL,
  `train_no` varchar(6) DEFAULT NULL,
  `remarks` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `date_indx` (`booking_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rms_rooms`
--

DROP TABLE IF EXISTS `rms_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rms_rooms` (
  `id` int NOT NULL AUTO_INCREMENT,
  `running_room_id` int DEFAULT NULL,
  `room_name` varchar(45) NOT NULL,
  `no_of_bed` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rms_running_rooms`
--

DROP TABLE IF EXISTS `rms_running_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rms_running_rooms` (
  `id` int NOT NULL AUTO_INCREMENT,
  `running_room_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sections` (
  `id` int NOT NULL AUTO_INCREMENT,
  `stn_sr_no` int DEFAULT NULL,
  `stn_code` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `section` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `blk_section` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `north_south` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `up_dn` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1167 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shednotice`
--

DROP TABLE IF EXISTS `shednotice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shednotice` (
  `id` int NOT NULL AUTO_INCREMENT,
  `train_date` date DEFAULT NULL,
  `shed` varchar(20) DEFAULT NULL,
  `link_type` varchar(20) DEFAULT NULL,
  `desig` varchar(5) DEFAULT NULL,
  `link_no` int DEFAULT NULL,
  `day_no` int DEFAULT NULL,
  `work_no` int DEFAULT NULL,
  `work_seq` int DEFAULT NULL,
  `duty_type` varchar(20) DEFAULT NULL,
  `t_no_num` int DEFAULT NULL,
  `n_emp_id` int DEFAULT NULL,
  `name` varchar(25) DEFAULT NULL,
  `org` varchar(10) DEFAULT NULL,
  `dest` varchar(10) DEFAULT NULL,
  `dep` datetime DEFAULT NULL,
  `arr` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx` (`train_date`,`shed`,`link_type`,`desig`)
) ENGINE=InnoDB AUTO_INCREMENT=383298 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `signal_positions`
--

DROP TABLE IF EXISTS `signal_positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `signal_positions` (
  `id` int NOT NULL,
  `imei` varchar(20) DEFAULT NULL,
  `imsi` varchar(20) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `rssi` double DEFAULT NULL,
  `batt` double DEFAULT NULL,
  `solar_vol` double DEFAULT NULL,
  `hw_v` double DEFAULT NULL,
  `sw_v` double DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `sensor` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ts` (`ts`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `signals`
--

DROP TABLE IF EXISTS `signals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `signals` (
  `id` int NOT NULL AUTO_INCREMENT,
  `section` varchar(20) DEFAULT NULL,
  `stn_code` varchar(20) DEFAULT NULL,
  `srl_no` int DEFAULT NULL,
  `imei` varchar(20) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `km_no` varchar(10) DEFAULT NULL,
  `left_right` varchar(50) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `up_dn` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1108 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stations`
--

DROP TABLE IF EXISTS `stations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stations` (
  `srl_no` int NOT NULL AUTO_INCREMENT,
  `stn_code` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `stn_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `stn_name_b` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `stn_name_h` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `phonetic` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`srl_no`),
  KEY `unique` (`stn_code`,`stn_name`)
) ENGINE=InnoDB AUTO_INCREMENT=265 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `supervisors`
--

DROP TABLE IF EXISTS `supervisors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `supervisors` (
  `emp_id` int NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Designation` varchar(50) DEFAULT NULL,
  `pf no` int DEFAULT NULL,
  `working at` varchar(50) DEFAULT NULL,
  `PASS_SRL` int DEFAULT NULL,
  `PME Done on` datetime(6) DEFAULT NULL,
  `PME Due on` datetime(6) DEFAULT NULL,
  `CUG_NO` varchar(15) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `firebase_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_attributes`
--

DROP TABLE IF EXISTS `tc_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_attributes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` varchar(4000) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `type` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `attribute` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `expression` varchar(4000) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_calendars`
--

DROP TABLE IF EXISTS `tc_calendars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_calendars` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `data` mediumblob NOT NULL,
  `attributes` varchar(4000) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_commands`
--

DROP TABLE IF EXISTS `tc_commands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_commands` (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` varchar(4000) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `type` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `textchannel` bit(1) NOT NULL DEFAULT b'0',
  `attributes` varchar(4000) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_device_attribute`
--

DROP TABLE IF EXISTS `tc_device_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_device_attribute` (
  `deviceid` int NOT NULL,
  `attributeid` int NOT NULL,
  KEY `fk_user_device_attribute_attributeid` (`attributeid`),
  KEY `fk_user_device_attribute_deviceid` (`deviceid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_device_command`
--

DROP TABLE IF EXISTS `tc_device_command`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_device_command` (
  `deviceid` int NOT NULL,
  `commandid` int NOT NULL,
  KEY `fk_device_command_commandid` (`commandid`),
  KEY `fk_device_command_deviceid` (`deviceid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_device_driver`
--

DROP TABLE IF EXISTS `tc_device_driver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_device_driver` (
  `deviceid` int NOT NULL,
  `driverid` int NOT NULL,
  KEY `fk_device_driver_deviceid` (`deviceid`),
  KEY `fk_device_driver_driverid` (`driverid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_device_geofence`
--

DROP TABLE IF EXISTS `tc_device_geofence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_device_geofence` (
  `deviceid` int NOT NULL,
  `geofenceid` int NOT NULL,
  KEY `fk_device_geofence_deviceid` (`deviceid`),
  KEY `fk_device_geofence_geofenceid` (`geofenceid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_device_maintenance`
--

DROP TABLE IF EXISTS `tc_device_maintenance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_device_maintenance` (
  `deviceid` int NOT NULL,
  `maintenanceid` int NOT NULL,
  KEY `fk_device_maintenance_deviceid` (`deviceid`),
  KEY `fk_device_maintenance_maintenanceid` (`maintenanceid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_device_notification`
--

DROP TABLE IF EXISTS `tc_device_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_device_notification` (
  `deviceid` int NOT NULL,
  `notificationid` int NOT NULL,
  KEY `fk_device_notification_deviceid` (`deviceid`),
  KEY `fk_device_notification_notificationid` (`notificationid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_devices`
--

DROP TABLE IF EXISTS `tc_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_devices` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `uniqueid` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `lastupdate` timestamp NULL DEFAULT NULL,
  `positionid` int DEFAULT NULL,
  `groupid` int DEFAULT NULL,
  `attributes` varchar(4000) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `phone` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `model` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `contact` varchar(512) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `category` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `disabled` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniqueid` (`uniqueid`),
  KEY `fk_devices_groupid` (`groupid`)
) ENGINE=InnoDB AUTO_INCREMENT=329 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_drivers`
--

DROP TABLE IF EXISTS `tc_drivers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_drivers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `uniqueid` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `attributes` varchar(4000) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniqueid` (`uniqueid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_events`
--

DROP TABLE IF EXISTS `tc_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_events` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `servertime` timestamp NOT NULL,
  `deviceid` int DEFAULT NULL,
  `positionid` int DEFAULT NULL,
  `geofenceid` int DEFAULT NULL,
  `attributes` varchar(4000) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `maintenanceid` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_events_deviceid` (`deviceid`)
) ENGINE=InnoDB AUTO_INCREMENT=8817167 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_geofences`
--

DROP TABLE IF EXISTS `tc_geofences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_geofences` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `description` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `area` varchar(4096) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `attributes` varchar(4000) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `calendarid` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_geofence_calendar_calendarid` (`calendarid`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_group_attribute`
--

DROP TABLE IF EXISTS `tc_group_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_group_attribute` (
  `groupid` int NOT NULL,
  `attributeid` int NOT NULL,
  KEY `fk_group_attribute_attributeid` (`attributeid`),
  KEY `fk_group_attribute_groupid` (`groupid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_group_command`
--

DROP TABLE IF EXISTS `tc_group_command`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_group_command` (
  `groupid` int NOT NULL,
  `commandid` int NOT NULL,
  KEY `fk_group_command_commandid` (`commandid`),
  KEY `fk_group_command_groupid` (`groupid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_group_driver`
--

DROP TABLE IF EXISTS `tc_group_driver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_group_driver` (
  `groupid` int NOT NULL,
  `driverid` int NOT NULL,
  KEY `fk_group_driver_driverid` (`driverid`),
  KEY `fk_group_driver_groupid` (`groupid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_group_geofence`
--

DROP TABLE IF EXISTS `tc_group_geofence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_group_geofence` (
  `groupid` int NOT NULL,
  `geofenceid` int NOT NULL,
  KEY `fk_group_geofence_geofenceid` (`geofenceid`),
  KEY `fk_group_geofence_groupid` (`groupid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_group_maintenance`
--

DROP TABLE IF EXISTS `tc_group_maintenance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_group_maintenance` (
  `groupid` int NOT NULL,
  `maintenanceid` int NOT NULL,
  KEY `fk_group_maintenance_groupid` (`groupid`),
  KEY `fk_group_maintenance_maintenanceid` (`maintenanceid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_group_notification`
--

DROP TABLE IF EXISTS `tc_group_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_group_notification` (
  `groupid` int NOT NULL,
  `notificationid` int NOT NULL,
  KEY `fk_group_notification_groupid` (`groupid`),
  KEY `fk_group_notification_notificationid` (`notificationid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_groups`
--

DROP TABLE IF EXISTS `tc_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_groups` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `groupid` int DEFAULT NULL,
  `attributes` varchar(4000) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_groups_groupid` (`groupid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_maintenances`
--

DROP TABLE IF EXISTS `tc_maintenances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_maintenances` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(4000) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `type` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `start` double NOT NULL DEFAULT '0',
  `period` double NOT NULL DEFAULT '0',
  `attributes` varchar(4000) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_notifications`
--

DROP TABLE IF EXISTS `tc_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_notifications` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `attributes` varchar(4000) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `always` bit(1) NOT NULL DEFAULT b'0',
  `calendarid` int DEFAULT NULL,
  `notificators` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notification_calendar_calendarid` (`calendarid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_positions`
--

DROP TABLE IF EXISTS `tc_positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_positions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `protocol` varchar(128) DEFAULT NULL,
  `deviceid` int NOT NULL,
  `servertime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `devicetime` timestamp NOT NULL,
  `fixtime` timestamp NOT NULL,
  `valid` bit(1) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `altitude` float NOT NULL,
  `speed` float NOT NULL,
  `course` float NOT NULL,
  `address` varchar(512) DEFAULT NULL,
  `attributes` varchar(4000) DEFAULT NULL,
  `accuracy` double NOT NULL DEFAULT '0',
  `network` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `position_deviceid_fixtime` (`deviceid`,`fixtime`),
  KEY `fixtime` (`fixtime`),
  CONSTRAINT `fk_positions_deviceid` FOREIGN KEY (`deviceid`) REFERENCES `tc_devices` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1195361515 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_servers`
--

DROP TABLE IF EXISTS `tc_servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_servers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `registration` bit(1) NOT NULL DEFAULT b'1',
  `latitude` double NOT NULL DEFAULT '0',
  `longitude` double NOT NULL DEFAULT '0',
  `zoom` int NOT NULL DEFAULT '0',
  `map` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `bingkey` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `mapurl` varchar(512) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `readonly` bit(1) NOT NULL DEFAULT b'0',
  `twelvehourformat` bit(1) NOT NULL DEFAULT b'0',
  `attributes` varchar(4000) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `forcesettings` bit(1) NOT NULL DEFAULT b'0',
  `coordinateformat` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `devicereadonly` bit(1) DEFAULT b'0',
  `limitcommands` bit(1) DEFAULT b'0',
  `poilayer` varchar(512) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_statistics`
--

DROP TABLE IF EXISTS `tc_statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_statistics` (
  `id` int NOT NULL AUTO_INCREMENT,
  `capturetime` timestamp NOT NULL,
  `activeusers` int NOT NULL DEFAULT '0',
  `activedevices` int NOT NULL DEFAULT '0',
  `requests` int NOT NULL DEFAULT '0',
  `messagesreceived` int NOT NULL DEFAULT '0',
  `messagesstored` int NOT NULL DEFAULT '0',
  `attributes` varchar(4096) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `mailsent` int NOT NULL DEFAULT '0',
  `smssent` int NOT NULL DEFAULT '0',
  `geocoderrequests` int NOT NULL DEFAULT '0',
  `geolocationrequests` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=756 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_user_attribute`
--

DROP TABLE IF EXISTS `tc_user_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_user_attribute` (
  `userid` int NOT NULL,
  `attributeid` int NOT NULL,
  KEY `fk_user_attribute_attributeid` (`attributeid`),
  KEY `fk_user_attribute_userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_user_calendar`
--

DROP TABLE IF EXISTS `tc_user_calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_user_calendar` (
  `userid` int NOT NULL,
  `calendarid` int NOT NULL,
  KEY `fk_user_calendar_calendarid` (`calendarid`),
  KEY `fk_user_calendar_userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_user_command`
--

DROP TABLE IF EXISTS `tc_user_command`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_user_command` (
  `userid` int NOT NULL,
  `commandid` int NOT NULL,
  KEY `fk_user_command_commandid` (`commandid`),
  KEY `fk_user_command_userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_user_device`
--

DROP TABLE IF EXISTS `tc_user_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_user_device` (
  `userid` int NOT NULL,
  `deviceid` int NOT NULL,
  KEY `fk_user_device_deviceid` (`deviceid`),
  KEY `fk_user_device_userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_user_driver`
--

DROP TABLE IF EXISTS `tc_user_driver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_user_driver` (
  `userid` int NOT NULL,
  `driverid` int NOT NULL,
  KEY `fk_user_driver_driverid` (`driverid`),
  KEY `fk_user_driver_userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_user_geofence`
--

DROP TABLE IF EXISTS `tc_user_geofence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_user_geofence` (
  `userid` int NOT NULL,
  `geofenceid` int NOT NULL,
  KEY `fk_user_geofence_geofenceid` (`geofenceid`),
  KEY `fk_user_geofence_userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_user_group`
--

DROP TABLE IF EXISTS `tc_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_user_group` (
  `userid` int NOT NULL,
  `groupid` int NOT NULL,
  KEY `fk_user_group_groupid` (`groupid`),
  KEY `fk_user_group_userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_user_maintenance`
--

DROP TABLE IF EXISTS `tc_user_maintenance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_user_maintenance` (
  `userid` int NOT NULL,
  `maintenanceid` int NOT NULL,
  KEY `fk_user_maintenance_maintenanceid` (`maintenanceid`),
  KEY `fk_user_maintenance_userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_user_notification`
--

DROP TABLE IF EXISTS `tc_user_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_user_notification` (
  `userid` int NOT NULL,
  `notificationid` int NOT NULL,
  KEY `fk_user_notification_notificationid` (`notificationid`),
  KEY `fk_user_notification_userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_user_user`
--

DROP TABLE IF EXISTS `tc_user_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_user_user` (
  `userid` int NOT NULL,
  `manageduserid` int NOT NULL,
  KEY `fk_user_user_userid` (`userid`),
  KEY `fk_user_user_manageduserid` (`manageduserid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tc_users`
--

DROP TABLE IF EXISTS `tc_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tc_users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `email` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `hashedpassword` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `salt` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `readonly` bit(1) NOT NULL DEFAULT b'0',
  `administrator` bit(1) DEFAULT NULL,
  `map` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `latitude` double NOT NULL DEFAULT '0',
  `longitude` double NOT NULL DEFAULT '0',
  `zoom` int NOT NULL DEFAULT '0',
  `twelvehourformat` bit(1) NOT NULL DEFAULT b'0',
  `attributes` varchar(4000) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `coordinateformat` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `disabled` bit(1) DEFAULT b'0',
  `expirationtime` timestamp NULL DEFAULT NULL,
  `devicelimit` int DEFAULT '-1',
  `token` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `userlimit` int DEFAULT '0',
  `devicereadonly` bit(1) DEFAULT b'0',
  `phone` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `limitcommands` bit(1) DEFAULT b'0',
  `login` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `poilayer` varchar(512) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_table`
--

DROP TABLE IF EXISTS `time_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `time_table` (
  `pk` bigint unsigned NOT NULL AUTO_INCREMENT,
  `train_no` int NOT NULL,
  `stn_name` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `stn_type` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `stn_code` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `division` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `zone` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `block_section` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `wtt_arr` time DEFAULT NULL,
  `wtt_day_arr` int DEFAULT NULL,
  `wtt_dep` time DEFAULT NULL,
  `wtt_day_dep` int DEFAULT NULL,
  `stop` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `ptt_arr` time DEFAULT NULL,
  `ptt_day_arr` int DEFAULT NULL,
  `ptt_dep` time DEFAULT NULL,
  `ptt_day_dep` int DEFAULT NULL,
  `pf` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `tr` int DEFAULT NULL,
  `er` int DEFAULT NULL,
  `distance` double DEFAULT NULL,
  `stn_sr_no` int DEFAULT NULL,
  PRIMARY KEY (`pk`),
  UNIQUE KEY `pk` (`pk`),
  UNIQUE KEY `train_no` (`train_no`,`stn_sr_no`),
  KEY `stn_code` (`stn_code`(4))
) ENGINE=InnoDB AUTO_INCREMENT=69224 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_table_n`
--

DROP TABLE IF EXISTS `time_table_n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `time_table_n` (
  `stn_sr_no` int NOT NULL,
  `stn_name` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `stn_type` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `stn_code` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `division` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `zone` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `block_section` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `wtt_arr` time DEFAULT NULL,
  `wtt_day_arr` int DEFAULT NULL,
  `wtt_dep` time DEFAULT NULL,
  `wtt_day_dep` int DEFAULT NULL,
  `stop` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `ptt_arr` time DEFAULT NULL,
  `ptt_day_arr` int DEFAULT NULL,
  `ptt_dep` time DEFAULT NULL,
  `ptt_day_dep` int DEFAULT NULL,
  `pf` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `tr` int DEFAULT NULL,
  `er` int DEFAULT NULL,
  `distance` double DEFAULT NULL,
  `train_no` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `pk` bigint unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`pk`),
  UNIQUE KEY `pk` (`pk`),
  KEY `train_no` (`train_no`)
) ENGINE=InnoDB AUTO_INCREMENT=26153 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `train_master`
--

DROP TABLE IF EXISTS `train_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `train_master` (
  `train_no` int NOT NULL,
  `train_name` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `train_type` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `valid_from` date DEFAULT NULL,
  `valid_to` date DEFAULT NULL,
  `zone` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `divn` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `depot` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `days_of_run` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `dep` time DEFAULT NULL,
  `org` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `dest` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `arr` time DEFAULT NULL,
  `travel_time` time DEFAULT NULL,
  `total_kms` decimal(10,0) DEFAULT NULL,
  `avg_speed` decimal(10,0) DEFAULT NULL,
  `er` int DEFAULT NULL,
  `tr` int DEFAULT NULL,
  PRIMARY KEY (`train_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `train_master_n`
--

DROP TABLE IF EXISTS `train_master_n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `train_master_n` (
  `train_no` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `train_name` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `train_type` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `valid_from` date DEFAULT NULL,
  `valid_to` date DEFAULT NULL,
  `zone` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `divn` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `depot` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `days_of_run` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `dep` time DEFAULT NULL,
  `org` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `dest` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `arr` time DEFAULT NULL,
  `travel_time` time DEFAULT NULL,
  `total_kms` decimal(10,0) DEFAULT NULL,
  `avg_speed` decimal(10,0) DEFAULT NULL,
  `er` int DEFAULT NULL,
  `tr` int DEFAULT NULL,
  `min_srl` int DEFAULT NULL,
  `max_srl` int DEFAULT NULL,
  PRIMARY KEY (`train_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `user_type` char(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `sup_id` int DEFAULT NULL,
  `sup_name` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Final view structure for view `last_train`
--

/*!50001 DROP VIEW IF EXISTS `last_train`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `last_train` AS select `a`.`device_id` AS `device_id`,`a`.`max_arr` AS `max_arr`,`b`.`id` AS `id`,`b`.`last_train_date` AS `last_train_date`,`b`.`last_train_no` AS `last_train_no`,`b`.`org` AS `org`,`b`.`dep` AS `dep`,`b`.`dest` AS `dest`,`b`.`arr` AS `arr` from ((select `device_train_map`.`device_id` AS `device_id`,max(`device_train_map`.`arr`) AS `max_arr` from `device_train_map` group by `device_train_map`.`device_id`) `a` left join (select `device_train_map`.`id` AS `id`,`device_train_map`.`device_id` AS `device_id`,`device_train_map`.`train_date` AS `last_train_date`,`device_train_map`.`train_no` AS `last_train_no`,`device_train_map`.`org` AS `org`,`device_train_map`.`dep` AS `dep`,`device_train_map`.`dest` AS `dest`,`device_train_map`.`arr` AS `arr` from `device_train_map`) `b` on(((`a`.`device_id` = `b`.`device_id`) and (`a`.`max_arr` = `b`.`arr`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-22 13:13:03
