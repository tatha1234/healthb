var moment = require('moment-timezone')

exports.getFormattedTime = function(t) {
  if (t !== '--') {
  //  var lt = moment.tz(t).format("YYYY-MM-DD HH:mm:ss")
    var dateUTC = new Date(t);
    var dateUTC = dateUTC.getTime()
    var dateIST = new Date(dateUTC);
    //date shifting for IST timezone (+5 hours and 30 minutes)
    dateIST.setHours(dateIST.getHours() + 5);
    dateIST.setMinutes(dateIST.getMinutes() + 30);
    return  moment(dateIST).format("HH:mm:ss")
  }
}
exports.getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

exports.bulkInsert = function(connection, table, objectArray, callback) {
  let keys = Object.keys(objectArray[0]);
  let values = objectArray.map(obj => keys.map(key => obj[key]));
  let updateString = objectArray.map(obj => keys.map(key => '`' + key + '`= VALUES(`' + key +'`)'));
  let sql = 'INSERT IGNORE INTO ' + table + ' (`'  + keys.join('`,`') + '`) VALUES ? ON DUPLICATE KEY UPDATE ' + updateString.join(',');
  console.log(sql)
  connection.query(sql, [values], function(error, results, fields) {
    if (connection) connection.release()
    if (error) {
      // console.log('bulkInsert', error)
      callback(error)
    } else {
       callback(null, results);
    }
  });
}

exports.bulkInsertUpdateDuplicate = function(connection, table, objectArray, callback) {
  let keys = Object.keys(objectArray[0]);
  let values = objectArray.map(obj => keys.map(key => obj[key]));
  let updateString = objectArray.map(obj => keys.map(key => `${key} = VALUES(${key})`));
  let sql = 'INSERT IGNORE INTO ' + table + ' (' + keys.join(',') + ') VALUES ? ON DUPLICATE KEY UPDATE ' + updateString.join(',');
  // console.log(sql, values)
  connection.query(sql, [values], function(error, results, fields) {
    if (connection) connection.release()
    if (error) {
      console.log('bulkInsertUpdateDuplicate', error)
      callback(error)
    } else {
      callback(null, results);
    }
  });
}


exports.round = function round(value, precision) {
    var multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
}
